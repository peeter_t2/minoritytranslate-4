package ee.translate.keeleleek.mtpluginframework;

/**
 * Specifies pull or spellcheck plugin version.
 */
public class PluginVersion {

	int major;
	int minor;
	
	
	// INIT
	public PluginVersion(int major, int minor) {
		super();
		this.major = major;
		this.minor = minor;
	}

	
	// VERSION
	int getMinor()
	 {
		return minor;
	 }
	
	public int getMajor()
	 {
		return major;
	 }
	
	public boolean isCompatibleWith(PluginVersion other)
	 {
		if (major != other.major) return false; // API incompatible
		return minor >= other.minor; // new API features, but backward compatible
	 }
	
	public boolean isLessThan(PluginVersion other)
	 {
		if (major < other.major) return true;
		if (minor < other.minor) return true;
		return false;
	 }
	
	
	// UTIL
	@Override
	public String toString() {
		return "" + major + '.' + minor;
	}
	
	
}
