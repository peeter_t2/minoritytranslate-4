package ee.translate.keeleleek.mtapplication.model.preferences;

import java.util.ArrayList;

import ee.translate.keeleleek.mtapplication.model.content.MinorityArticle;
import ee.translate.keeleleek.mtapplication.model.processing.RegexCollect;

/**
 * Immutable preferences for collect.
 *
 */
public class CollectPreferences {

	private ArrayList<CollectEntry> collects = new ArrayList<>();

	
	/* ******************
	 *                  *
	 *  Initialization  *
	 *                  *
	 ****************** */
	public CollectPreferences()
	 {
		this.collects = new ArrayList<>();
	 }
	
	public CollectPreferences(ArrayList<CollectEntry> collects)
	 {
		this.collects = collects;
	 }

	public CollectPreferences(CollectPreferences other)
	 {
		this.collects = new ArrayList<>(other.collects.size());
		for (int i = 0; i < other.collects.size(); i++) {
			this.collects.add(new CollectEntry(other.collects.get(i)));
		}
	 }
	
	public static CollectPreferences create()
	 {
		CollectPreferences preferences = new CollectPreferences();
		return preferences;
	 }
	

	
	/* ******************
	 *                  *
	 *   Preferences    *
	 *                  *
	 ****************** */
	public int getCollectCount() {
		return collects.size();
	}
	
	public CollectEntry getCollect(int i) {
		return collects.get(i);
	}
	
	public ArrayList<RegexCollect> filterCollects(MinorityArticle srcArticle, MinorityArticle dstArticle)
	 {
		String srcLangCode = srcArticle.getRef().getLangCode();
		String dstLangCode = dstArticle.getRef().getLangCode();
		
		return filterCollects(srcLangCode, dstLangCode);
	 }

	public ArrayList<RegexCollect> filterCollects(String srcLangCode, String dstLangCode)
	 {
		ArrayList<RegexCollect> result = new ArrayList<>();
		for (CollectEntry entry : collects) {
			if (entry.getFilter().isAccept(srcLangCode, dstLangCode)) result.add(entry.createRegex());
		}
		
		return result;
	 }

	
	
	/* ******************
	 *                  *
	 *     Utility      *
	 *                  *
	 ****************** */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((collects == null) ? 0 : collects.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CollectPreferences other = (CollectPreferences) obj;
		if (collects == null) {
			if (other.collects != null)
				return false;
		} else if (!collects.equals(other.collects))
			return false;
		return true;
	}
	
	
}
