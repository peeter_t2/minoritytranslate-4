package ee.translate.keeleleek.mtapplication.view.dialogs;

public interface ConfirmDeclineDialog {

	public void prepare();
	public void focus();
	public void confirm();
	public void decline();
	
}
