package ee.translate.keeleleek.mtapplication.view.javafx.views;

import java.util.List;

import ee.translate.keeleleek.mtapplication.view.elements.EditorMediator;
import ee.translate.keeleleek.mtapplication.view.elements.EditorMediator.EditorPosition;
import ee.translate.keeleleek.mtapplication.view.javafx.editors.AlignEditorMediatorFX;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.input.ScrollEvent;

public class AlignViewMediatorFX extends ViewMediatorFX {

	private final static int EDIT_COUNT = 4;

	@FXML
	private Button startButton;
	@FXML
	private Button upUpButton;
	@FXML
	private Button upButton;
	@FXML
	private Label indexLabel;
	@FXML
	private Button downButton;
	@FXML
	private Button downDownButton;
	@FXML
	private Button endButton;
	
	private int editorIndex = 0;
	
	
	/* ******************
	 *                  *
	 *  Initialization  *
	 *                  *
	 ****************** */
	public AlignViewMediatorFX(ViewMode viewMode)
	 {
		super(viewMode);
	 }
	
	@FXML
	private void initialize()
	 {
		//super.initialize();
		
		refreshControls();
	 }
	
	/* ******************
	 *                  *
	 *     Refresh      *
	 *                  *
	 ****************** */
	private void refreshControls()
	 {
		startButton.setDisable(editorIndex <= 0);
		upButton.setDisable(editorIndex <= 0);
		upUpButton.setDisable(upButton.isDisable());
		
		indexLabel.setText("" + editorIndex);
	 }
	
	
	
	/* ******************
	 *                  *
	 *      Index       *
	 *                  *
	 ****************** */
	public void changeEditorIndex(int editorIndex)
	 {
		if (editorIndex < 0) editorIndex = 0;
		if (this.editorIndex == editorIndex) return;
		
		this.editorIndex = editorIndex;
		
		List<EditorMediator> editors;
		
		editors = getEditors(EditorPosition.DESTINATION);
		for (EditorMediator editorMediator : editors) {
			((AlignEditorMediatorFX) editorMediator).flushText();
			((AlignEditorMediatorFX) editorMediator).changeEditorIndex(editorIndex);
		}
		
		editors = getEditors(EditorPosition.SOURCE);
		for (EditorMediator editorMediator : editors) {
			((AlignEditorMediatorFX) editorMediator).flushText();
			((AlignEditorMediatorFX) editorMediator).changeEditorIndex(editorIndex);
		}
		
		refreshControls();
	 }
	
	public int findMaximumIndex()
	 {
		int index = -1;
		
		List<EditorMediator> editors;
		
		editors = getEditors(EditorPosition.DESTINATION);
		for (EditorMediator editorMediator : editors) {
			int i = ((AlignEditorMediatorFX) editorMediator).findEditorMaxIndex();
			if (i > index) index = i;
		}
		
		editors = getEditors(EditorPosition.SOURCE);
		for (EditorMediator editorMediator : editors) {
			int i = ((AlignEditorMediatorFX) editorMediator).findEditorMaxIndex();
			if (i > index) index = i;
		}
		
		return index;
	}
	
	
	/* ******************
	 *                  *
	 *      Events      *
	 *                  *
	 ****************** */
	@Override
	protected void onSelectedArticleChanged() {
		if (editorIndex != 0) changeEditorIndex(0);
	}
	
	@FXML
	public void onStartClick() {
		changeEditorIndex(0);
	}
	
	@FXML
	public void onUpUpClick() {
		changeEditorIndex(editorIndex - EDIT_COUNT);
	}
	
	@FXML
	public void onUpClick() {
		changeEditorIndex(editorIndex - 1);
	}

	@FXML
	public void onDownClick() {
		changeEditorIndex(editorIndex + 1);
	}

	@FXML
	public void onDownDownClick() {
		changeEditorIndex(editorIndex + EDIT_COUNT);
	}

	@FXML
	public void onEndClick() {
		int i = findMaximumIndex();
		if (i != -1) changeEditorIndex(i);
	}
	
	@FXML
	public void onMouseScroll(ScrollEvent event)
	 {
		double d = event.getDeltaY();
		if (d == 0.0) d = event.getDeltaX();
		
		if (d < 0 && !downButton.isDisabled()) { // down
			onDownClick();
		}
		else if (d > 0 && !upButton.isDisabled()) { // up
			onUpClick();
		}
	 }
	
	
}
