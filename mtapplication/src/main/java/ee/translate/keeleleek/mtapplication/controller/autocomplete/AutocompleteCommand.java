package ee.translate.keeleleek.mtapplication.controller.autocomplete;

import org.puremvc.java.multicore.interfaces.INotification;
import org.puremvc.java.multicore.patterns.command.SimpleCommand;

import ee.translate.keeleleek.mtapplication.model.MinorityTranslateModel;
import ee.translate.keeleleek.mtapplication.model.autocomplete.AutocompleteRequest;

public class AutocompleteCommand extends SimpleCommand {

	
	@Override
	public void execute(INotification notification)
	 {
		AutocompleteRequest request = (AutocompleteRequest) notification.getBody();
		
		MinorityTranslateModel.autocomplete().request(request);
	 }
	
}
