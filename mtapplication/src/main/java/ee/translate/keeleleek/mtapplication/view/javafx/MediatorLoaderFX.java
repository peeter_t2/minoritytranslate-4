package ee.translate.keeleleek.mtapplication.view.javafx;

import java.io.IOException;
import java.net.URL;

import org.puremvc.java.multicore.patterns.mediator.Mediator;

import ee.translate.keeleleek.mtapplication.view.dialogs.AboutDialogMediator;
import ee.translate.keeleleek.mtapplication.view.dialogs.AddContentDialogMediator;
import ee.translate.keeleleek.mtapplication.view.dialogs.ConfirmationDialogMediator;
import ee.translate.keeleleek.mtapplication.view.dialogs.ListsDialogMediator;
import ee.translate.keeleleek.mtapplication.view.dialogs.LoginDialogMediator;
import ee.translate.keeleleek.mtapplication.view.dialogs.ManualDialogMediator;
import ee.translate.keeleleek.mtapplication.view.dialogs.MessageDialogMediator;
import ee.translate.keeleleek.mtapplication.view.dialogs.PullMappingsDialogMediator;
import ee.translate.keeleleek.mtapplication.view.dialogs.QuickStartDialogMediator;
import ee.translate.keeleleek.mtapplication.view.dialogs.SimpleDialogMediator;
import ee.translate.keeleleek.mtapplication.view.dialogs.UnicodeTableDialogMediator;
import ee.translate.keeleleek.mtapplication.view.elements.EditorMediator;
import ee.translate.keeleleek.mtapplication.view.javafx.dialogs.PreferencesDialogMediatorFX;
import ee.translate.keeleleek.mtapplication.view.javafx.dialogs.SimpleDialogMediatorFX;
import ee.translate.keeleleek.mtapplication.view.javafx.pages.InsertsPageMediatorFX;
import ee.translate.keeleleek.mtapplication.view.messages.Messages;
import ee.translate.keeleleek.mtapplication.view.pages.CollectPageMediator;
import ee.translate.keeleleek.mtapplication.view.pages.ContentAssistPageMediator;
import ee.translate.keeleleek.mtapplication.view.pages.FindAddonPageMediator;
import ee.translate.keeleleek.mtapplication.view.pages.InsertsPageMediator;
import ee.translate.keeleleek.mtapplication.view.pages.LookupAddonPageMediator;
import ee.translate.keeleleek.mtapplication.view.pages.LookupsPageMediator;
import ee.translate.keeleleek.mtapplication.view.pages.ReplacePageMediator;
import ee.translate.keeleleek.mtapplication.view.pages.SymbolsPageMediator;
import ee.translate.keeleleek.mtapplication.view.pages.TemplateMappingPageMediator;
import ee.translate.keeleleek.mtapplication.view.views.ViewMediator;
import ee.translate.keeleleek.mtapplication.view.windows.TranslateWindowMediator;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.control.SplitPane;
import javafx.scene.layout.HBox;

public class MediatorLoaderFX {

	public static Mediator loadMediator(String mediatorName, String fxmlPath)
	 {
		URL url = MediatorLoaderFX.class.getResource(fxmlPath);
		FXMLLoader loader = new FXMLLoader(url, Messages.getResourceBundle());

		Parent parent;
		try {
			parent = (Parent) loader.load();
		} catch (IOException e) {
			e.printStackTrace();
			return null;
		}
		
		Mediator mediator = (Mediator) loader.getController();
		mediator.setViewComponent(parent);
		
		return mediator;
	 }
	
	public static Parent loadMediator(Object controller, String fxmlPath)
	 {
		URL url = MediatorLoaderFX.class.getResource(fxmlPath);
		FXMLLoader loader = new FXMLLoader(url, Messages.getResourceBundle());
		loader.setController(controller);

		Parent parent;
		try {
			parent = (Parent) loader.load();
		} catch (IOException e) {
			e.printStackTrace();
			return null;
		}
		
		Mediator mediator = (Mediator) loader.getController();
		mediator.setViewComponent(parent);
		
		return parent;
	 }
	
	public static Mediator loadSimpleDialogMediator(String name, String fxmlPath)
	 {
		URL url = MediatorLoaderFX.class.getResource(fxmlPath);
		FXMLLoader loader = new FXMLLoader(url, Messages.getResourceBundle());
		loader.setController(new SimpleDialogMediatorFX(name));
		
		Parent parent;
		try {
			parent = (Parent) loader.load();
		} catch (IOException e) {
			e.printStackTrace();
			return null;
		}
		
		SimpleDialogMediatorFX mediator = (SimpleDialogMediatorFX) loader.getController();
		mediator.setViewComponent(parent);
		
		return mediator;
	 }
	
	
	// WINDOWS:
	public static Mediator loadTranslateWindowMediator() {
		return loadMediator(TranslateWindowMediator.NAME, "/fxview/TranslateWindow.fxml");
	}

	public static Mediator loadLoginWindowMediator() {
		return loadMediator(LoginDialogMediator.NAME, "/fxview/LoginWindow.fxml");
	}

	public static Mediator loadPreferencesWindowMediator() {
		return loadMediator(PreferencesDialogMediatorFX.NAME, "/fxview/PreferencesDialog.fxml");
	}

	public static Mediator loadAddCategoryWindowMediator() {
		return loadSimpleDialogMediator(SimpleDialogMediator.NAME_ADD_CATEGORY, "/fxview/SimpleDialogWindow.fxml");
	}

	public static Mediator loadAddArticleWindowMediator() {
		return loadSimpleDialogMediator(SimpleDialogMediator.NAME_ADD_ARTICLE, "/fxview/SimpleDialogWindow.fxml");
	}

	public static Mediator loadRemoveArticleWindowMediator() {
		return loadSimpleDialogMediator(SimpleDialogMediator.NAME_REMOVE_ARTICLE, "/fxview/SimpleDialogWindow.fxml");
	}

	public static Mediator loadAddLinksWindowMediator() {
		return loadSimpleDialogMediator(SimpleDialogMediator.NAME_ADD_LINKS, "/fxview/SimpleDialogWindow.fxml");
	}

	public static Mediator loadAboutWindowMediator() {
		return loadMediator(AboutDialogMediator.NAME, "/fxview/AboutWindow.fxml");
	}

	public static Mediator loadManualWindowMediator() {
		return loadMediator(ManualDialogMediator.NAME, "/fxview/ManualWindow.fxml");
	}

	public static Mediator loadListsWindowMediator() {
		return loadMediator(ListsDialogMediator.NAME, "/fxview/ListsWindow.fxml");
	}

	public static Mediator loadSnippetsPaneMediator() {
		return loadMediator(ListsDialogMediator.NAME, "/fxview/SnippetsPane.fxml");
	}

	public static LookupAddonPageMediator loadLookupAddonPageMediator() {
		return (LookupAddonPageMediator) loadMediator(LookupAddonPageMediator.NAME, "/fxview/LookupAddonPage.fxml");
	}

	public static FindAddonPageMediator loadFindAddonPageMediator() {
		return (FindAddonPageMediator) loadMediator(FindAddonPageMediator.NAME, "/fxview/FindAddonPage.fxml");
	}

	public static Mediator loadPullProgressMediator() {
		return loadMediator(TranslateWindowMediator.NAME, "/fxview/PullProgressDialog.fxml");
	}

	public static Mediator loadPullMappingsDialogMediator() {
		return loadMediator(PullMappingsDialogMediator.NAME, "/fxview/PullMappingsDialog.fxml");
	}

	
	// DIALOGS
	public static QuickStartDialogMediator loadQuickStartDialogMediator() {
		return (QuickStartDialogMediator) loadMediator(QuickStartDialogMediator.NAME, "/fxview/QuickStartDialog.fxml");
	}
	
	public static UnicodeTableDialogMediator loadUnicodeDialogMediator() {
		return (UnicodeTableDialogMediator) loadMediator(UnicodeTableDialogMediator.NAME, "/fxview/UnicodeTableDialog.fxml");
	}
	
	public static AddContentDialogMediator loadAddContentDialogMediator() {
		return (AddContentDialogMediator) loadMediator(AddContentDialogMediator.NAME, "/fxview/AddContentDialog.fxml");
	}
	
	
	// PAGES
	public static CollectPageMediator loadCollectPage() {
		return (CollectPageMediator) loadMediator(CollectPageMediator.NAME, "/fxview/CollectPage.fxml");
	}
	public static ReplacePageMediator loadReplacePage() {
		return (ReplacePageMediator) loadMediator(CollectPageMediator.NAME, "/fxview/ReplacePage.fxml");
	}
	
	public static InsertsPageMediatorFX loadInsertsPage() {
		return (InsertsPageMediatorFX) loadMediator(InsertsPageMediator.NAME, "/fxview/InsertsPage.fxml");
	}

	public static TemplateMappingPageMediator loadTemplateMappingPage() {
		return (TemplateMappingPageMediator) loadMediator(TemplateMappingPageMediator.NAME, "/fxview/TemplateMappingPage.fxml");
	}

	public static ContentAssistPageMediator loadContentAssistPage() {
		return (ContentAssistPageMediator) loadMediator(InsertsPageMediator.NAME, "/fxview/ContentAssistPage.fxml");
	}

	public static SymbolsPageMediator loadSymbolsPage() {
		return (SymbolsPageMediator) loadMediator(SymbolsPageMediator.NAME, "/fxview/SymbolsPage.fxml");
	}

	public static LookupsPageMediator loadLookupsPage() {
		return (LookupsPageMediator) loadMediator(LookupsPageMediator.NAME, "/fxview/LookupsPage.fxml");
	}

	
	// VIEWS
	public static SplitPane loadSplitView(ViewMediator controller)
	 {
		switch (controller.getViewMode()) {
		
		case SPLIT_VERTICAL:
			return (SplitPane) loadMediator(controller, "/fxview/VerticalView.fxml");
		
		case SPLIT_HORIZONTAL:
			return (SplitPane) loadMediator(controller, "/fxview/HorizontalView.fxml");

		default:
			return null;
		}
	 }

	public static HBox loadAlignView(ViewMediator controller)
	 {
		return (HBox) loadMediator(controller, "/fxview/AlignView.fxml");
	 }

	public static Parent loadSingleView(ViewMediator controller)
	 {
		return (Parent) loadMediator(controller, "/fxview/SingleView.fxml");
	 }
	
	
	// EDITORS
	public static Parent loadVerticalEditorMediator(EditorMediator mediator) {
		return loadMediator(mediator, "/fxview/VerticalEditor.fxml");
	}

	public static Parent loadHorizontalEditorMediator(EditorMediator mediator) {
		return loadMediator(mediator, "/fxview/HorizontalEditor.fxml");
	}

	public static Parent loadAlignEditorMediator(EditorMediator mediator) {
		return loadMediator(mediator, "/fxview/AlignEditor.fxml");
	}

	public static Parent loadSingleEditorMediator(EditorMediator mediator) {
		return loadMediator(mediator, "/fxview/SingleEditor.fxml");
	}
	

	// PANES:
	public static MessageDialogMediator loadMessageWindowMediator() {
		return (MessageDialogMediator) loadMediator(MessageDialogMediator.NAME, "/fxview/MessageWindow.fxml");
	}

	public static ConfirmationDialogMediator loadConfirmationDialogMediator() {
		return (ConfirmationDialogMediator) loadMediator(ConfirmationDialogMediator.NAME, "/fxview/ConfirmationDialog.fxml");
	}

	
}
