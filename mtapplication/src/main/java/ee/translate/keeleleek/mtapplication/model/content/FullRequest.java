package ee.translate.keeleleek.mtapplication.model.content;

public class FullRequest {

	private String qid;
	private String langCode;
	private Namespace namespace;
	private String wikiTitle;
	
	
	// INIT
	public FullRequest(String qid, String langCode, Namespace namespace, String wikiTitle) throws NullPointerException
	 {
		if (qid == null) throw new NullPointerException("qid cannot be null");
		if (langCode == null) throw new NullPointerException("langCode cannot be null");
		if (namespace == null) throw new NullPointerException("namespace cannot be null");
		if (wikiTitle == null) wikiTitle = "";
		
		this.qid = qid;
		this.langCode = langCode;
		this.namespace = namespace;
		this.wikiTitle = wikiTitle;		
	 }

	
	// CONVERSIONS
	public Reference createReference() {
		return new Reference(qid, langCode);
	}
	
	public WikiReference createWikiReference() throws NullPointerException {
		return new WikiReference(langCode, namespace, wikiTitle);
	}
	

	// VALUES
	public String getQid() {
		return qid;
	}
	
	public String getLangCode() {
		return langCode;
	}
	
	public String getWikiTitle() {
		return wikiTitle;
	}


	// WORKINGS
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((langCode == null) ? 0 : langCode.hashCode());
		result = prime * result + ((qid == null) ? 0 : qid.hashCode());
		result = prime * result + ((wikiTitle == null) ? 0 : wikiTitle.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj)
	 {
		if (this == obj) return true;
		if (obj == null) return false;
		if (getClass() != obj.getClass()) return false;
		FullRequest other = (FullRequest) obj;
		if (langCode == null) {
			if (other.langCode != null) return false;
		} else if (!langCode.equals(other.langCode))
			return false;
		if (qid == null) {
			if (other.qid != null) return false;
		} else if (!qid.equals(other.qid)) return false;
		if (wikiTitle == null) {
			if (other.wikiTitle != null) return false;
		} else if (!wikiTitle.equals(other.wikiTitle)) return false;
		return true;
	 }

	@Override
	public String toString() {
		return "{qid=" + qid + " langCode=" + langCode + " wikiTitle=" + wikiTitle + "}";
	}

	
}
