package ee.translate.keeleleek.mtapplication.model.autocomplete;

import ee.translate.keeleleek.mtapplication.model.content.Reference;

public class AutocompleteRequest {

	private Reference ref;
	private String preceedingText;
	private String selectedText;
	
	
	public AutocompleteRequest(Reference ref, String preceedingText, String selectedText) {
		super();
		this.ref = ref;
		this.preceedingText = preceedingText;
		this.selectedText = selectedText;
	}
	

	public Reference getRef() {
		return ref;
	}

	public String getPreceeding() {
		return preceedingText;
	}

	public String getSelected() {
		return selectedText;
	}

	
}
