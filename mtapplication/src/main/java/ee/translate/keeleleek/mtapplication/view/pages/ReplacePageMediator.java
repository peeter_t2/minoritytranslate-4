package ee.translate.keeleleek.mtapplication.view.pages;

import org.puremvc.java.multicore.patterns.mediator.Mediator;

import ee.translate.keeleleek.mtapplication.model.preferences.ReplacePreferences;


public abstract class ReplacePageMediator extends Mediator implements PreferencesPage<ReplacePreferences> {

	public final static String NAME = "{38F40A2F-2DC8-4811-A86A-3B3AB4824936}";
	
	
	// INIT
	public ReplacePageMediator() {
		super(NAME, null);
	}

	@Override
	public void onRegister() {
		
	}

	
}
