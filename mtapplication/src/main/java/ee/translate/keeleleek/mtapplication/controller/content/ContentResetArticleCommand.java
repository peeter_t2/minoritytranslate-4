package ee.translate.keeleleek.mtapplication.controller.content;

import org.puremvc.java.multicore.interfaces.INotification;
import org.puremvc.java.multicore.patterns.command.SimpleCommand;

import ee.translate.keeleleek.mtapplication.model.MinorityTranslateModel;
import ee.translate.keeleleek.mtapplication.model.content.MinorityArticle;
import ee.translate.keeleleek.mtapplication.model.content.MinorityArticle.Status;
import ee.translate.keeleleek.mtapplication.model.content.Reference;
import ee.translate.keeleleek.mtapplication.view.dialogs.DialogFactory;

public class ContentResetArticleCommand extends SimpleCommand {

	@Override
	public void execute(INotification notification)
	 {
		Reference ref = (Reference) notification.getBody();
		
		MinorityArticle article = MinorityTranslateModel.content().getArticle(ref);
		if (article == null) return;
		
		if (!DialogFactory.dialogs().confirmReset()) return;
		
		if (article.isNew()) MinorityTranslateModel.content().changeTitle(ref, "");
		MinorityTranslateModel.content().changeStatus(ref, Status.DOWNLOAD_REQUESTED);
	 }
	
}
