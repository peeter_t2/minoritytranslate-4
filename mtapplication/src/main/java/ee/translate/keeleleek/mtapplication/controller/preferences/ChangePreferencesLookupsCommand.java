package ee.translate.keeleleek.mtapplication.controller.preferences;

import org.puremvc.java.multicore.interfaces.INotification;
import org.puremvc.java.multicore.patterns.command.SimpleCommand;

import ee.translate.keeleleek.mtapplication.model.MinorityTranslateModel;
import ee.translate.keeleleek.mtapplication.model.preferences.LookupsPreferences;

public class ChangePreferencesLookupsCommand extends SimpleCommand {

	@Override
	public void execute(INotification notification)
	 {
		LookupsPreferences preferences = (LookupsPreferences) notification.getBody();
		
		MinorityTranslateModel.preferences().changeLookups(preferences);
	 }
	
}
