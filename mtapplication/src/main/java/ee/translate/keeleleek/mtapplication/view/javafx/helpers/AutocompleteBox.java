package ee.translate.keeleleek.mtapplication.view.javafx.helpers;

import ee.translate.keeleleek.mtapplication.model.autocomplete.AutocompleteChoice;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;
import javafx.scene.paint.Color;

public class AutocompleteBox extends HBox {

	private AutocompleteChoice choise;

	
	public AutocompleteBox(AutocompleteChoice choise) {
		getChildren().add(new Label(choise.getName()));
		if (!choise.getDescription().isEmpty()) {
			Label descLabel = new Label(" - " + choise.getDescription());
			descLabel.setTextFill(Color.GRAY);
			getChildren().add(descLabel);
		}
		this.choise = choise;
	}

	public AutocompleteChoice getChoise() {
		return choise;
	}
	
	
}
