package ee.translate.keeleleek.mtapplication.model.lists;

public interface SuggestionList {

	public final static String GROUP_UNGROUPED = "Ungrouped";
	
	public String getName();
	public String getGroup();
	
}
