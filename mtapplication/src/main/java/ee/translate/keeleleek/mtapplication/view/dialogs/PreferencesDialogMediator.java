package ee.translate.keeleleek.mtapplication.view.dialogs;

import org.puremvc.java.multicore.interfaces.INotification;
import org.puremvc.java.multicore.patterns.mediator.Mediator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ee.translate.keeleleek.mtapplication.Notifications;
import ee.translate.keeleleek.mtapplication.model.MinorityTranslateModel;
import ee.translate.keeleleek.mtapplication.model.preferences.CollectPreferences;
import ee.translate.keeleleek.mtapplication.model.preferences.ContentAssistPreferences;
import ee.translate.keeleleek.mtapplication.model.preferences.CorpusPreferences;
import ee.translate.keeleleek.mtapplication.model.preferences.InsertsPreferences;
import ee.translate.keeleleek.mtapplication.model.preferences.LanguagesPreferences;
import ee.translate.keeleleek.mtapplication.model.preferences.LookupsPreferences;
import ee.translate.keeleleek.mtapplication.model.preferences.PreferencesProxy.Display;
import ee.translate.keeleleek.mtapplication.model.preferences.PreferencesProxy.Proficiency;
import ee.translate.keeleleek.mtapplication.model.preferences.ProgramPreferences;
import ee.translate.keeleleek.mtapplication.model.preferences.ReplacePreferences;
import ee.translate.keeleleek.mtapplication.model.preferences.SymbolsPreferences;
import ee.translate.keeleleek.mtapplication.model.preferences.TemplateMappingPreferences;
import ee.translate.keeleleek.mtapplication.view.application.ApplicationMediator;
import ee.translate.keeleleek.mtapplication.view.messages.Messages;
import ee.translate.keeleleek.mtapplication.view.pages.CollectPageMediator;
import ee.translate.keeleleek.mtapplication.view.pages.ContentAssistPageMediator;
import ee.translate.keeleleek.mtapplication.view.pages.InsertsPageMediator;
import ee.translate.keeleleek.mtapplication.view.pages.LookupsPageMediator;
import ee.translate.keeleleek.mtapplication.view.pages.ReplacePageMediator;
import ee.translate.keeleleek.mtapplication.view.pages.SymbolsPageMediator;
import ee.translate.keeleleek.mtapplication.view.pages.TemplateMappingPageMediator;


public abstract class PreferencesDialogMediator extends Mediator implements ConfirmDeclineDialog {

	final public static String NAME = "{ABC1B283-5CFF-4D88-8522-65D3E84E5CFD}";
	
	protected static Logger LOGGER = LoggerFactory.getLogger(PreferencesDialogMediator.class);

	
	protected ProgramPreferences program = null;
	protected LanguagesPreferences languages = null;
	protected CorpusPreferences corpus = null;

	protected CollectPageMediator collectPageMediator = null;
	protected ReplacePageMediator replacePageMediator = null;
	protected TemplateMappingPageMediator templateMappingMediator = null;
	protected ContentAssistPageMediator contentAssistMediator = null;
	protected InsertsPageMediator templatesMediator = null;
	protected SymbolsPageMediator symbolsMediator = null;
	protected LookupsPageMediator lookupsMediator = null;
	
	protected boolean resetting = false;
	protected boolean manual = false;
	
	protected String[] guiLangCodes = Messages.getLangCodes();
	protected String[] guiLangNames = collectGUILanguages();;
	
	
	// INIT
	public PreferencesDialogMediator() {
		super(NAME, null);
	}
	
	@Override
	public void onRegister() {
		
	}
	

	// NOTIFICATIONS
	@Override
	public String[] listNotificationInterests() {
		return new String[]
		 {
			Notifications.WINDOW_PREFERENCES_PREPARE,
			Notifications.SESSION_LOADED,
			Notifications.CONNECTION_STATUS_CHANGED,
			Notifications.DOWNLOADER_STARTED,
			Notifications.DOWNLOADER_ENDED
		 };
	}
	
	@Override
	public void handleNotification(INotification notification)
	 {
		switch (notification.getName()) {
		case Notifications.WINDOW_PREFERENCES_PREPARE:
			break;
				
		 default:
			break;
		}
	 }
	
	
	// REFRESH
	private void refreshProgram()
	 {
		program = new ProgramPreferences(MinorityTranslateModel.preferences().program());
		initProgram(program);
	 }
	
	private void refreshLanguages()
	 {
		languages = new LanguagesPreferences(MinorityTranslateModel.preferences().languages());
		initLanguages(languages);
	 }

	private void refreshCorpus()
	 {
		corpus = new CorpusPreferences(MinorityTranslateModel.preferences().corpus());
		initCorpus(languages, corpus);
	 }

	
	// INHERIT (INIT)
	protected abstract void initProgram(ProgramPreferences program);
	protected abstract void initLanguages(LanguagesPreferences languages);
	protected abstract void initCorpus(LanguagesPreferences languages, CorpusPreferences corpus);

	protected abstract void openTemplateMapping(TemplateMappingPreferences preferences);
	protected abstract TemplateMappingPreferences closeTemplateMapping();

	protected abstract void openContentAssist(ContentAssistPreferences preferences);
	protected abstract ContentAssistPreferences closeContentAssist();

	protected abstract void openInserts(InsertsPreferences preferences);
	protected abstract InsertsPreferences closeInserts();

	protected abstract void openSymbols(SymbolsPreferences prefernces);
	protected abstract SymbolsPreferences closeSymbols();

	protected abstract void openLookups(LookupsPreferences prefernces);
	protected abstract LookupsPreferences closeLookups();

	
	// EVENTS (PROGRAM)
	public void onCategoryDepthChange(Integer depth) {
		program.setCategoryDepth(depth);
	}

	public void onTemplatesFilterChange(boolean filter) {
		program.setTemplatesFilter(filter);
	}

	public void onFilesFilterChange(boolean filter) {
		program.setFilesFilter(filter);
	}

	public void onIntroductionFilterChange(boolean filter) {
		program.setIntroductionFilter(filter);
	}

	public void onReferencesFilterChange(boolean filter) {
		program.setReferencesFilter(filter);
	}

	public void onGUILangCodeChange(String langCode) {
		program.setGuiLangCode(langCode);
	}
	
	
	// EVENTS (LANGUAGES)
	public void onRequestCorpusPage() {
		ApplicationMediator mediator = (ApplicationMediator) getFacade().retrieveMediator(ApplicationMediator.NAME);
		mediator.showDocument(Messages.getString("preferences.program.corpus.link"));
	}
	
	public void onAddLanguage(String lang)
	 {
		String langCode = MinorityTranslateModel.wikis().findLangCode(lang);
		if (langCode == null) return;
		
		languages.addLangCode(langCode);
		initLanguages(languages);

		initCorpus(languages, corpus);
	 }
	
	public void onRemoveLanguage(String langCode)
	 {
		languages.removeLangCode(langCode);
		initLanguages(languages);
		
		initCorpus(languages, corpus);
	 }
	
	public void onDisplayChange(String langCode, Display display) {
		languages.changeDisplay(langCode, display);
	}
	
	
	// EVENTS (CORPUS)
	public void onProficiencyChange(String langCode, Proficiency display) {
		corpus.changeProficiency(langCode, display);
	}
	
	public void onCollectCorpusChange(Boolean collect)
	 {
		corpus.changeCollect(collect);
	 }

	public void onExactCorpusChange(boolean exact)
	 {
		corpus.changeExact(exact);
	 }

	
	// EVENTS (PROCESING)
	public void onRequestProcessingPage() {
		ApplicationMediator mediator = (ApplicationMediator) getFacade().retrieveMediator(ApplicationMediator.NAME);
		mediator.showDocument(Messages.getString("preferences.processing.link"));
	}
	
	
	// INHERIT (CONFIRM DECLINE DIALOG)
	public void prepare()
	 {
		refreshProgram();
		refreshLanguages();
		refreshCorpus();
	 }
	
	@Override
	public void focus()
	 {
		
	 }
	
	@Override
    public void confirm()
	 {
		// preferences update
		if (program != null) sendNotification(Notifications.PREFERENCES_CHANGE_PROGRAM, program);
		if (languages != null) sendNotification(Notifications.PREFERENCES_CHANGE_LANGUAGES, languages);
		if (corpus != null) sendNotification(Notifications.PREFERENCES_CHANGE_CORPUS, corpus);
		
		// close pages
		if (collectPageMediator!= null) {
			CollectPreferences preferences = collectPageMediator.close();
			collectPageMediator = null;
			sendNotification(Notifications.PREFERENCES_CHANGE, preferences);
		}
		
		if (replacePageMediator!= null) {
			ReplacePreferences preferences = replacePageMediator.close();
			replacePageMediator = null;
			sendNotification(Notifications.PREFERENCES_CHANGE, preferences);
		}
		
		TemplateMappingPreferences templatesMappingPreferences = closeTemplateMapping();
		ContentAssistPreferences contentAssistPreferences = closeContentAssist();
		InsertsPreferences insertsPreferences = closeInserts();
		SymbolsPreferences symbolsPreferences = closeSymbols();
		LookupsPreferences lookupsPreferences = closeLookups();
		
		if (templatesMappingPreferences != null) sendNotification(Notifications.PREFERENCES_CHANGE_TEMPLATE_MAPPING, templatesMappingPreferences);
		if (contentAssistPreferences != null) sendNotification(Notifications.PREFERENCES_CHANGE_CONTENT_ASSIST, contentAssistPreferences);
		if (insertsPreferences != null) sendNotification(Notifications.PREFERENCES_CHANGE_INSERTS, insertsPreferences);
		if (symbolsPreferences != null) sendNotification(Notifications.PREFERENCES_CHANGE_SYMBOLS, symbolsPreferences);
		if (lookupsPreferences != null) sendNotification(Notifications.PREFERENCES_CHANGE_LOOKUPS, lookupsPreferences);
		
		// Notify preference changes:
		sendNotification(Notifications.MENU_PREFERENCES_CONFIRMED);
		
		// Save preferences:
		sendNotification(Notifications.PREFERENCES_SAVE);
     }

	@Override
    public void decline()
     {
		// close pages
    	closeInserts();
    	closeContentAssist();
		closeSymbols();
		closeLookups();
     }

	
	// HELPERS
	public String[] collectGUILanguages()
	 {
		String[] guiLangNames = new String[guiLangCodes.length];
		for (int i = 0; i < guiLangNames.length; i++) {
			String langName = MinorityTranslateModel.wikis().getLangName(guiLangCodes[i]);
			if (langName == null) langName = guiLangCodes[i];
			guiLangNames[i] = langName;
		}
		
		return guiLangNames;
	 }
	
	
}
