package ee.translate.keeleleek.mtapplication.view.javafx.dialogs;

import java.util.ArrayList;

import ee.translate.keeleleek.mtapplication.Notifications;
import ee.translate.keeleleek.mtapplication.model.MinorityTranslateModel;
import ee.translate.keeleleek.mtapplication.model.preferences.ContentAssistPreferences;
import ee.translate.keeleleek.mtapplication.model.preferences.CorpusPreferences;
import ee.translate.keeleleek.mtapplication.model.preferences.InsertsPreferences;
import ee.translate.keeleleek.mtapplication.model.preferences.LanguagesPreferences;
import ee.translate.keeleleek.mtapplication.model.preferences.LookupsPreferences;
import ee.translate.keeleleek.mtapplication.model.preferences.PreferencesProxy.Display;
import ee.translate.keeleleek.mtapplication.model.preferences.PreferencesProxy.Proficiency;
import ee.translate.keeleleek.mtapplication.model.preferences.ProgramPreferences;
import ee.translate.keeleleek.mtapplication.model.preferences.SymbolsPreferences;
import ee.translate.keeleleek.mtapplication.model.preferences.TableEntry;
import ee.translate.keeleleek.mtapplication.model.preferences.TemplateMappingPreferences;
import ee.translate.keeleleek.mtapplication.view.dialogs.PreferencesDialogMediator;
import ee.translate.keeleleek.mtapplication.view.javafx.MediatorLoaderFX;
import ee.translate.keeleleek.mtapplication.view.listeners.IntegerKeyEventHandler;
import ee.translate.keeleleek.mtapplication.view.messages.Messages;
import ee.translate.keeleleek.mtapplication.view.suggestions.Suggestable;
import ee.translate.keeleleek.mtapplication.view.suggestions.SuggestionClickEventHandler;
import javafx.application.Platform;
import javafx.beans.InvalidationListener;
import javafx.beans.Observable;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.geometry.Side;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.Label;
import javafx.scene.control.MenuItem;
import javafx.scene.control.RadioButton;
import javafx.scene.control.SplitPane;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.Tooltip;
import javafx.scene.control.TreeItem;
import javafx.scene.control.TreeView;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.StackPane;


public class PreferencesDialogMediatorFX extends PreferencesDialogMediator {

	@FXML
	private Node rootNode;
	@FXML
	private SplitPane treeContentSplit;
	@FXML
	private TreeView<String> preferencesTree;
	private TreeItem<String> programItem;
	private TreeItem<String> languagesItem;
	private TreeItem<String> corpusItem;
	private TreeItem<String> processingItem;
	private TreeItem<String> collectItem;
	private TreeItem<String> replaceItem;
	private TreeItem<String> templateMappingItem;
	private TreeItem<String> contentAssistItem;
	private TreeItem<String> insertsItem;
	private TreeItem<String> symbolsItem;
	private TreeItem<String> lookupsItem;
	private TreeItem<String> lastItem = null;
	
	@FXML
	private StackPane pages;
	
	@FXML
	private Node programPage;
	@FXML
	private TextField categoryDepth;
	@FXML
	private CheckBox templatesFilterCheckBox;
	@FXML
	private CheckBox filesFilterCheckBox;
	@FXML
	private CheckBox introductionFilterCheckBox;
	@FXML
	private CheckBox referencesFilterCheckBox;
	@FXML
	private ComboBox<String> guiLanguageCombo;
	@FXML
	private ComboBox<String> fontSizeCombobox;

	@FXML
	private Node languagesPage;
	@FXML
	private GridPane languagesPane;
	@FXML
	private TextField languagesAddTextField;
	@FXML
	private ContextMenu languagesContextMenu;
	@FXML
	private Button languagesAddButton;
	@FXML
	private Button languagesRemoveButton;
	
	@FXML
	private Node processingPage;
	
	@FXML
	private Node corpusPage;
	@FXML
	private GridPane corpusPane;
	@FXML
	private CheckBox collectCorpusCheckbox;
	@FXML
	private RadioButton corpusExactRadiobutton;
	@FXML
	private RadioButton corpusAdaptationRadiobutton;
	
	@FXML
	private TableView<TableEntry> collectTable;
	
	@FXML
	private TableView<TableEntry> replaceTable;

	private Node collectPage = null;
	private Node replacePage = null;
	private Node templateMappingPage = null;
	private Node contentAssistPage = null;
	private Node insertsPage = null;
	private Node symbolsPage = null;
	private Node lookupsPage = null;
	
	@FXML
	private Suggestable languageSuggestable = new Suggestable() {
		
		@Override
		public void update(String suggested) {
			manual = true;
			TextField textField = languagesAddTextField;
			textField.setText(suggested);
			textField.positionCaret(suggested.length());
			manual = false;
		}
		
		@Override
		public void suggest(String[] results) {
			ContextMenu contextMenu = languagesContextMenu;
			TextField textField = languagesAddTextField;
			if (textField == null) return;
			
			contextMenu.getItems().clear();
			
			for (int i = 0; i < results.length; i++) {
				MenuItem item = new MenuItem(results[i]);
				item.setOnAction(new SuggestionClickEventHandler(results[i], this));
				contextMenu.getItems().add(item);
			}
			
			if (results.length > 0){
				contextMenu.show(textField, Side.BOTTOM, 0, 0);
			} else {
				contextMenu.hide();
			}
		}
		
		@Override
		public String getSearchTerm() {
			return languagesAddTextField.getText();
		}
	
	};
	
	
	// INIT
	public PreferencesDialogMediatorFX() {
		super();
	}
	
	@Override
	public void onRegister()
	 {
		super.onRegister();

		// decline on close
		/*rootNode.getScene().getWindow().setOnCloseRequest(new EventHandler<WindowEvent>() {
			@Override
			public void handle(WindowEvent event) {
				onDecline();
			}
		});*/
		
		// program
		languagesAddTextField.textProperty().addListener(new InvalidationListener() {
			@Override
			public void invalidated(Observable arg0) {
				if (!resetting && !manual) sendNotification(Notifications.SUGGEST_LANGUAGE, languageSuggestable);
			}
		});
		
		categoryDepth.addEventFilter(KeyEvent.KEY_TYPED, new IntegerKeyEventHandler());
		categoryDepth.textProperty().addListener(new InvalidationListener() {
			@Override
			public void invalidated(Observable observable) {
				onCategoryDepthChange();
			}
		});
		
		// tree
		TreeItem<String> rootItem = new TreeItem<String>("Root");
		preferencesTree.setRoot(rootItem);
		preferencesTree.setShowRoot(false);

		preferencesTree.getSelectionModel().selectedItemProperty().addListener(new InvalidationListener() {
			@Override
			public void invalidated(Observable observable) {
				show(preferencesTree.getSelectionModel().selectedItemProperty().getValue());
			}
		});

		// program
		programItem = new TreeItem<String>(Messages.getString("preferences.program"));
		programItem.setExpanded(true);
		rootItem.getChildren().add(programItem);

		languagesItem = new TreeItem<String>(Messages.getString("preferences.program.languages"));
		programItem.getChildren().add(languagesItem);

		corpusItem = new TreeItem<String>(Messages.getString("preferences.program.corpus"));
		programItem.getChildren().add(corpusItem);

		guiLanguageCombo.getItems().clear();
		for (int i = 0; i < guiLangNames.length; i++) {
			guiLanguageCombo.getItems().add(guiLangNames[i]);
		}
		
		// processing
		processingItem = new TreeItem<String>(Messages.getString("preferences.processing.page"));
		processingItem.setExpanded(true);
		rootItem.getChildren().add(processingItem);

		collectItem = new TreeItem<String>(Messages.getString("preferences.processing.collect.page"));
		processingItem.getChildren().add(collectItem);

		replaceItem = new TreeItem<String>(Messages.getString("preferences.processing.replace.page"));
		processingItem.getChildren().add(replaceItem);

		templateMappingItem = new TreeItem<String>(Messages.getString("preferences.processing.template.mapping.page"));
		processingItem.getChildren().add(templateMappingItem);
		
		// content assist
		contentAssistItem = new TreeItem<String>(Messages.getString("preferences.content.assist"));
		contentAssistItem.setExpanded(true);
		rootItem.getChildren().add(contentAssistItem);

		insertsItem = new TreeItem<String>(Messages.getString("preferences.content.assist.inserts"));
		contentAssistItem.getChildren().add(insertsItem);

		symbolsItem = new TreeItem<String>(Messages.getString("preferences.content.assist.symbols"));
		contentAssistItem.getChildren().add(symbolsItem);
		
		// lookups
		lookupsItem = new TreeItem<String>(Messages.getString("preferences.lookups"));
		lookupsItem.setExpanded(true);
		rootItem.getChildren().add(lookupsItem);

		// select
		preferencesTree.getSelectionModel().select(0);
		Platform.runLater(new Runnable() {
			@Override
			public void run()
			 {
				preferencesTree.getFocusModel().focus(0);
			 }
		});
	 }

	@FXML
	private void initialize()
	 {
		// program
		fontSizeCombobox.getItems().addAll(ProgramPreferences.FONT_SIZES);
	 }
	
	
	// PREPARE
	@Override
	public void prepare()
	 {
		super.prepare();
		if (lastItem != null) show(lastItem);
	 }
	
	
	// INHERIT (INIT)
	@Override
	protected void initProgram(ProgramPreferences program)
	 {
		categoryDepth.setText(program.getCategoryDepth().toString());
		
		templatesFilterCheckBox.setSelected(program.isTemplatesFilter());
		filesFilterCheckBox.setSelected(program.isFilesFilter());
		introductionFilterCheckBox.setSelected(program.isIntroductionFilter());
		referencesFilterCheckBox.setSelected(program.isReferencesFilter());
		
		int selected = -1;
		for (int i = 0; i < guiLangCodes.length; i++) {
			if (guiLangCodes[i].equals(program.getGUILangCode())) selected = i;
		}
		if (selected != -1) guiLanguageCombo.getSelectionModel().select(selected);
		
		fontSizeCombobox.getSelectionModel().select(MinorityTranslateModel.preferences().getFontSize());
	 }
	
	@Override
	protected void initLanguages(LanguagesPreferences languages)
	 {
		ArrayList<String> langCodes = languages.getLangCodes();
		
		languagesPane.getChildren().clear();

		languagesPane.addRow(0, new Label(Messages.getString("preferences.program.languages.language")), new Label(Messages.getString("preferences.program.languages.display")));
		
		if (langCodes.size() == 0) languagesPane.addRow(1, new Label("-"), new Label("-"), new Label("-"));
		
		for (int i = 0; i < langCodes.size(); i++) {
			
			final String langCode = langCodes.get(i);
			Display display = languages.getDisplay(langCode);
			
			// language name
			String langName = MinorityTranslateModel.wikis().getLangName(langCode);
			if (langName == null) langName = langCode;
			Label langLabel1 = new Label(langName);
			
			// display
			ObservableList<String> dispList = FXCollections.observableArrayList(
					Messages.getString("preferences.program.languages.display.from"),
					Messages.getString("preferences.program.languages.display.to"),
					Messages.getString("preferences.program.languages.display.none")
			);
			final ComboBox<String> dispCombo = new ComboBox<>(dispList);
			dispCombo.setMaxWidth(Double.MAX_VALUE);
			dispCombo.getSelectionModel().select(display.ordinal());
			dispCombo.setTooltip(new Tooltip(Messages.getString("preferences.program.languages.display.tooltip")));
			dispCombo.getSelectionModel().selectedIndexProperty().addListener(new InvalidationListener() {
				@Override
				public void invalidated(Observable observable) {
					onDisplayChange(langCode, Display.values()[dispCombo.getSelectionModel().getSelectedIndex()]);
				}
			});
			
			// remove button
			Button removeButton = new Button(Messages.getString("preferences.program.languages.buttons.remove"));
			removeButton.setOnAction(new EventHandler<ActionEvent>() {
				@Override
				public void handle(ActionEvent event) {
					onRemoveLanguage(langCode);
				}
			});
			
			languagesPane.addRow(i + 1, langLabel1, dispCombo, removeButton);
			
		}
	 }

	@Override
	protected void initCorpus(LanguagesPreferences languages, CorpusPreferences corpus)
	 {
		ArrayList<String> langCodes = languages.getLangCodes();
		
		corpusPane.getChildren().clear();

		corpusPane.addRow(0, new Label(Messages.getString("preferences.program.languages.language")), new Label(Messages.getString("preferences.program.languages.display")));
		
		if (langCodes.size() == 0) corpusPane.addRow(1, new Label("-"), new Label("-"), new Label("-"));
		
		for (int i = 0; i < langCodes.size(); i++) {
			
			final String langCode = langCodes.get(i);
			Proficiency display = corpus.getProficiency(langCode);
			
			// language name
			String langName = MinorityTranslateModel.wikis().getLangName(langCode);
			if (langName == null) langName = langCode;
			Label langLabel1 = new Label(langName);
			
			// display
			ObservableList<String> dispList = FXCollections.observableArrayList(
					Messages.getString("preferences.program.corpus.proficiency.unspecified"),
					Messages.getString("preferences.program.corpus.proficiency.level1"),
					Messages.getString("preferences.program.corpus.proficiency.level2"),
					Messages.getString("preferences.program.corpus.proficiency.level3"),
					Messages.getString("preferences.program.corpus.proficiency.level4"),
					Messages.getString("preferences.program.corpus.proficiency.level5")
			);
			final ComboBox<String> dispCombo = new ComboBox<>(dispList);
			dispCombo.setMaxWidth(Double.MAX_VALUE);
			dispCombo.getSelectionModel().select(display.ordinal());
			dispCombo.getSelectionModel().selectedIndexProperty().addListener(new InvalidationListener() {
				@Override
				public void invalidated(Observable observable) {
					onProficiencyChange(langCode, Proficiency.values()[dispCombo.getSelectionModel().getSelectedIndex()]);
				}
			});
			
			corpusPane.addRow(i + 1, langLabel1, dispCombo);
			
		}
		
		collectCorpusCheckbox.setSelected(corpus.isCollect());
		
		// exact
		if (corpus.isExact()) corpusExactRadiobutton.setSelected(true);
		else corpusAdaptationRadiobutton.setSelected(true);
	 }

	@Override
	protected void openTemplateMapping(TemplateMappingPreferences preferences)
	 {
		if (templateMappingMediator == null) {
			templateMappingMediator = MediatorLoaderFX.loadTemplateMappingPage();
			getFacade().registerMediator(templateMappingMediator);
		}
		
		if (templateMappingPage == null) {
			templateMappingPage = (Node) templateMappingMediator.getViewComponent();
			pages.getChildren().add(templateMappingPage);
		}
		
		templateMappingMediator.open(preferences);
	 }
	
	@Override
	protected TemplateMappingPreferences closeTemplateMapping()
	 {
		TemplateMappingPreferences preferences = null;
		
		if (templateMappingPage != null) {
			pages.getChildren().remove(templateMappingPage);
			templateMappingPage = null;
		}
		
		if (templateMappingMediator != null) {
			getFacade().removeMediator(templateMappingMediator.getMediatorName());
			preferences = templateMappingMediator.close();
			templateMappingMediator = null;
		}
		
		return preferences;
	 }
	
	@Override
	protected void openContentAssist(ContentAssistPreferences preferences)
	 {
		if (contentAssistMediator == null) {
			contentAssistMediator = MediatorLoaderFX.loadContentAssistPage();
			getFacade().registerMediator(contentAssistMediator);
		}
		
		if (contentAssistPage == null) {
			contentAssistPage = (Node) contentAssistMediator.getViewComponent();
			pages.getChildren().add(contentAssistPage);
		}
		
		contentAssistMediator.open(preferences);
	 }
	
	@Override
	protected ContentAssistPreferences closeContentAssist()
	 {
		ContentAssistPreferences preferences = null;
		
		if (contentAssistPage != null) {
			pages.getChildren().remove(contentAssistPage);
			contentAssistPage = null;
		}
		
		if (contentAssistMediator != null) {
			getFacade().removeMediator(contentAssistMediator.getMediatorName());
			preferences = contentAssistMediator.close();
			contentAssistMediator = null;
		}
		
		return preferences;
	 }
	

	@Override
	protected void openInserts(InsertsPreferences preferences)
	 {
		if (templatesMediator == null) {
			templatesMediator = MediatorLoaderFX.loadInsertsPage();
			getFacade().registerMediator(templatesMediator);
		}
		
		if (insertsPage == null) {
			insertsPage = (Node) templatesMediator.getViewComponent();
			pages.getChildren().add(insertsPage);
		}
		
		templatesMediator.open(preferences);
	 }
	
	@Override
	protected InsertsPreferences closeInserts()
	 {
		InsertsPreferences preferences = null;
		
		if (insertsPage != null) {
			pages.getChildren().remove(insertsPage);
			insertsPage = null;
		}
		
		if (templatesMediator != null) {
			getFacade().removeMediator(templatesMediator.getMediatorName());
			preferences = templatesMediator.close();
			templatesMediator = null;
		}
		
		return preferences;
	 }
	

	@Override
	protected void openSymbols(SymbolsPreferences preferences)
	 {
		if (symbolsMediator == null) {
			symbolsMediator = MediatorLoaderFX.loadSymbolsPage();
			getFacade().registerMediator(symbolsMediator);
		}
		
		if (symbolsPage == null) {
			symbolsPage = (Node) symbolsMediator.getViewComponent();
			pages.getChildren().add(symbolsPage);
		}
		
		symbolsMediator.open(preferences);
	 }
	
	@Override
	protected SymbolsPreferences closeSymbols()
	 {
		SymbolsPreferences preferences = null;
		
		if (symbolsPage != null) {
			pages.getChildren().remove(symbolsPage);
			symbolsPage = null;
		}
		
		if (symbolsMediator != null) {
			getFacade().removeMediator(symbolsMediator.getMediatorName());
			preferences = symbolsMediator.close();
			symbolsMediator = null;
		}
		
		return preferences;
	 }
	

	@Override
	protected void openLookups(LookupsPreferences preferences)
	 {
		if (lookupsMediator == null) {
			lookupsMediator = MediatorLoaderFX.loadLookupsPage();
			getFacade().registerMediator(lookupsMediator);
			lookupsMediator.open(preferences);
		}
		
		if (lookupsPage == null) {
			lookupsPage = (Node) lookupsMediator.getViewComponent();
			pages.getChildren().add(lookupsPage);
		}
	 }
	
	@Override
	protected LookupsPreferences closeLookups()
	 {
		LookupsPreferences preferences = null;
		
		if (lookupsPage != null) {
			pages.getChildren().remove(lookupsPage);
			lookupsPage = null;
		}
		
		if (lookupsMediator != null) {
			getFacade().removeMediator(lookupsMediator.getMediatorName());
			preferences = lookupsMediator.close();
			lookupsMediator = null;
		}
		
		return preferences;
	 }
	
	
	// INHERIT HELPERS
	private void show(TreeItem<String> item)
	 {
		programPage.setVisible(item == programItem);
		languagesPage.setVisible(item == languagesItem);
		corpusPage.setVisible(item == corpusItem);
		processingPage.setVisible(item == processingItem);

		if (item == templateMappingItem) openTemplateMapping(MinorityTranslateModel.preferences().getTemplateMapping());
		if (templateMappingPage != null) templateMappingPage.setVisible(item == templateMappingItem);

		if (item == contentAssistItem) openContentAssist(MinorityTranslateModel.preferences().getContentAssist());
		if (contentAssistPage != null) contentAssistPage.setVisible(item == contentAssistItem);

		if (item == insertsItem) openInserts(MinorityTranslateModel.preferences().getInserts());
		if (insertsPage != null) insertsPage.setVisible(item == insertsItem);

		if (item == symbolsItem) openSymbols(MinorityTranslateModel.preferences().getSymbols());
		if (symbolsPage != null) symbolsPage.setVisible(item == symbolsItem);

		if (item == lookupsItem) openLookups(MinorityTranslateModel.preferences().getLookups());
		if (lookupsPage != null) lookupsPage.setVisible(item == lookupsItem);

		if (item == collectItem) {
			if (collectPageMediator == null) { // create mediator
				collectPageMediator = MediatorLoaderFX.loadCollectPage();
				collectPageMediator.open(MinorityTranslateModel.preferences().collects());
			}
			
			collectPage = (Node) collectPageMediator.getViewComponent(); // add page
			if (!pages.getChildren().contains(collectPage)) {
				pages.getChildren().add(collectPage);
			}
		} else {
			if (collectPage != null) { // remove page
				pages.getChildren().remove(collectPage);
				collectPage = null;
			}
		}

		if (item == replaceItem) {
			if (replacePageMediator == null) { // create mediator
				replacePageMediator = MediatorLoaderFX.loadReplacePage();
				replacePageMediator.open(MinorityTranslateModel.preferences().replaces());
			}
			
			replacePage = (Node) replacePageMediator.getViewComponent(); // add page
			if (!pages.getChildren().contains(replacePage)) {
				pages.getChildren().add(replacePage);
			}
		} else {
			if (replacePage != null) { // remove page
				pages.getChildren().remove(replacePage);
				replacePage = null;
			}
		}
		
		lastItem = item;
	 }
	
	
	// EVENTS (PROGRAM)
	@FXML
    public void onCategoryDepthChange() {
		super.onCategoryDepthChange(Integer.parseInt(categoryDepth.getText()));
    }

	@FXML
    public void onTemplatesFilterCheck() {
		super.onTemplatesFilterChange(templatesFilterCheckBox.isSelected());
    }

	@FXML
    public void onFilesFilterCheck() {
		super.onFilesFilterChange(filesFilterCheckBox.isSelected());
    }

	@FXML
    public void onIntroductionFilterCheck() {
		super.onIntroductionFilterChange(introductionFilterCheckBox.isSelected());
    }

	@FXML
    public void onReferencesFilterCheck() {
		super.onReferencesFilterChange(referencesFilterCheckBox.isSelected());
    }

	@FXML
    public void onGUILanguageChange() {
		String langCode = guiLangCodes[guiLanguageCombo.getSelectionModel().getSelectedIndex()];
		super.onGUILangCodeChange(langCode);
    }

	@FXML
    public void onFontSizeChange() {
		if (program != null) program.setFontSize(fontSizeCombobox.getSelectionModel().getSelectedItem());
    }

	
	// EVENTS (LANGUAGES)
	@FXML
	public void onAddLanguageClick()
	 {
		super.onAddLanguage(languagesAddTextField.getText());
		languagesAddTextField.setText("");
     }

	
	// EVENTS (CORPUS)
	@FXML
	public void onRequestCorpusPage() {
		super.onRequestCorpusPage();
	}
	
	@FXML
    public void onCollectCorpusCheck() {
		super.onCollectCorpusChange(collectCorpusCheckbox.isSelected());
    }

	@FXML
    public void onDefaultExactCheck() {
		super.onExactCorpusChange(true);
    }

	@FXML
    public void onDefaultAdaptationCheck() {
		super.onExactCorpusChange(false);
    }

	
	
	// EVENTS (PROCESSING)
	@FXML
	public void onRequestProcessingPage() {
		super.onRequestProcessingPage();
	}
	
	
}
