package ee.translate.keeleleek.mtapplication.model.plugins;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;

import ee.translate.keeleleek.mtapplication.controller.actions.EntitiesAction;
import ee.translate.keeleleek.mtapplication.controller.actions.RedirectsAction;
import ee.translate.keeleleek.mtapplication.model.MinorityTranslateModel;
import ee.translate.keeleleek.mtapplication.model.content.Namespace;
import ee.translate.keeleleek.mtapplication.model.preferences.TemplateMapping;
import ee.translate.keeleleek.mtapplication.view.messages.Messages;
import ee.translate.keeleleek.mtpluginframework.MinorityTranslatePlugins;
import ee.translate.keeleleek.mtpluginframework.PluginVersion;
import ee.translate.keeleleek.mtpluginframework.chopup.RootElement;
import ee.translate.keeleleek.mtpluginframework.chopup.TemplateElement;
import ee.translate.keeleleek.mtpluginframework.chopup.TemplateParameterElement;
import ee.translate.keeleleek.mtpluginframework.chopup.TextBlockElement;
import ee.translate.keeleleek.mtpluginframework.chopup.WikilinkElement;
import ee.translate.keeleleek.mtpluginframework.pull.PullCallback;
import ee.translate.keeleleek.mtpluginframework.pull.PullPlugin;

public class CopyReplacePastePlugin implements PullPlugin {

	public final static String PLUGIN_NAME = "Copy Replace Paste";
	public final static PluginVersion PLUGIN_VERSION = new PluginVersion(1, 1);
	
	private HashMap<String, HashMap<String,String>> linkMapCache = new HashMap<>();
	
	
	public String getName() {
		return PLUGIN_NAME;
	}
	
	@Override
	public String getName(String langCode) {
		return Messages.getString("plugins.copy.replace.paste.name");
	}

	public PluginVersion getPullVersion() {
		return PLUGIN_VERSION;
	}

	
	@Override
	public void setup(MinorityTranslatePlugins plugins) {
		
	}

	@Override
	public void close() {
		
	}
	
	
	@Override
	public boolean pull(String srcLangCode, String dstLangCode, String srcTitle, RootElement srcRoot, String dstTitle, String dstText, PullCallback progress) throws Exception
	 {
		ArrayList<WikilinkElement> wikilinks = new ArrayList<>();
		ArrayList<TemplateElement> templates = new ArrayList<>();
		
		if (MinorityTranslateModel.preferences().isTranslateWikilinks()) {
			srcRoot.collectWikilinks(wikilinks);
		}
		
		if (MinorityTranslateModel.preferences().isTranslateTemplates()) {
			srcRoot.collectTemplates(templates);
		}
		
		int total = 0;
		total+= wikilinks.size();
		total+= templates.size();
		total+= 50;
		progress.init(total);
		
		if (!wikilinks.isEmpty()) translateWikilinks(srcLangCode, dstLangCode, wikilinks, progress);
		if (!templates.isEmpty()) translateTemplates(srcLangCode, dstLangCode, templates, progress);
		
		ArrayList<TextBlockElement> textBlocks = new ArrayList<>();
		srcRoot.collectTextBlocks(textBlocks);
		progress.advance(50);
		
		return !progress.isDeclined();
	}


	private void translateWikilinks(String srcLangCode, String dstLangCode, List<WikilinkElement> wikilinks, PullCallback progress)
	 {
		progress.action(Messages.getString("plugins.pull.traslating.wikilinks"));
		
		HashMap<String, String> cache = retrieveCache(srcLangCode, dstLangCode);
		HashMap<String,String> linkMap = new HashMap<>();
		ArrayList<String> srcLinks = new ArrayList<>();
		
		// collect
		for (WikilinkElement wikilink : wikilinks) {
			
			if (progress.isDeclined()) return;
			
			String srcLink = wikilink.getLink();
			
			if (linkMap.containsKey(srcLink)) {
				progress.advance();
				continue; 
			}
			
			if (cache.containsKey(srcLink)) {
				linkMap.put(srcLink, cache.get(srcLink));
				progress.advance();
				continue;
			}
			
			srcLinks.add(srcLink);
			
			if (srcLinks.size() >= EntitiesAction.MAX_TITLE_COUNT) {
				fetch(srcLangCode, dstLangCode, srcLinks, linkMap);
				progress.advance(srcLinks.size());
				srcLinks.clear();
			}

		}

		if (srcLinks.size() > 0) {
			fetch(srcLangCode, dstLangCode, srcLinks, linkMap);
			progress.advance(srcLinks.size());
			srcLinks.clear();
		}
		
		// replace
		for (WikilinkElement wikilink : wikilinks) {
			
			String key = wikilink.getLink();
			String value = linkMap.get(key);

			String namespace = wikilink.getNamespace();
			String link = wikilink.getLink();
			
			if (value == null) {
				progress.map(namespace, link, wikilink);
				continue;
			}
			
			if (startsLowerCase(key)) value = lowerFirstCase(value);
			wikilink.setLink(value);
			
			progress.map(namespace, link, wikilink);

		}
		
		// namespaces
		for (WikilinkElement wikilink : wikilinks) {
			
			String srcNamespace = wikilink.getNamespace();
			
			Namespace namespace = MinorityTranslateModel.wikis().findNamespace(srcLangCode, srcNamespace);
			if (namespace == null && !srcLangCode.equals("en")) MinorityTranslateModel.wikis().findNamespace("en", srcNamespace);
			if (namespace == null) continue;
			
			String dstNamespace = MinorityTranslateModel.wikis().findNamespace(dstLangCode, namespace);
			if (dstNamespace != null) wikilink.setNamespace(dstNamespace);
			
		}
		
		// cache
		cache.putAll(linkMap);
	 }
	
	private void translateTemplates(String srcLangCode, String dstLangCode, List<TemplateElement> templates, PullCallback progress)
	 {
		progress.action(Messages.getString("plugins.pull.traslating.templates"));
		
		HashMap<String, String> cache = retrieveCache(srcLangCode, dstLangCode);
		HashMap<String,String> templateMap = new HashMap<>();
		ArrayList<String> srcTemplates = new ArrayList<>();
		
		// collect
		for (TemplateElement template : templates) {
			
			if (progress.isDeclined()) return;
			
			String srcLink = "Template:" + template.getName();
			
			if (templateMap.containsKey(srcLink)) {
				progress.advance();
				continue; 
			}
			
			if (cache.containsKey(srcLink)) {
				templateMap.put(srcLink, cache.get(srcLink));
				progress.advance();
				continue;
			}
			
			srcTemplates.add(srcLink);
			
			if (srcTemplates.size() >= EntitiesAction.MAX_TITLE_COUNT) {
				fetch(srcLangCode, dstLangCode, srcTemplates, templateMap);
				progress.advance(srcTemplates.size());
				srcTemplates.clear();
			}
			
		}

		if (srcTemplates.size() > 0) {
			fetch(srcLangCode, dstLangCode, srcTemplates, templateMap);
			progress.advance(srcTemplates.size());
			srcTemplates.clear();
		}
		
		// replace
		for (TemplateElement template : templates) {
			
			String key = "Template:" + template.getName();
			String value = templateMap.get(key);
			
			String source = template.getName();
			
			remapTemplateParameters(srcLangCode, dstLangCode, template.getName(), template, progress);
				
			if (value == null) {
				progress.map(source, template);
				continue;
			}
			
			int i = key.indexOf(':');
			if (i != -1) key = key.substring(i + 1);
			
			int j = value.indexOf(':');
			if (j != -1) value = value.substring(j + 1);
			
			template.setName(value);
			progress.map(source, template);

		}
		
		// cache
		cache.putAll(templateMap);
	 }
	

	private void remapTemplateParameters(String srcLangCode, String dstLangCode, String srcTemplate, TemplateElement template, PullCallback progress)
	 {
		ArrayList<String> parameterNames = new ArrayList<>();
		ArrayList<TemplateParameterElement> elements = new ArrayList<>();
		
		for (int i = 0; i < template.getParameterCount(); i++) {
			
			TemplateParameterElement element = template.getParameter(i);
			String parameterName = element.getName();
			
			parameterNames.add(parameterName);
			elements.add(element);
			
		}
		
		List<TemplateMapping> mappings = MinorityTranslateModel.preferences().getTemplateMapping().findMappings(srcLangCode, dstLangCode, srcTemplate);
		
		for (TemplateMapping mapping : mappings) {
			
			if (mapping.getSrcParameter().isEmpty()) { // create a new one
				
				String parameterName = "";
				TemplateParameterElement element = template.createParameter(mapping.getDstParameter());
				if (element != null) {
					parameterNames.add(parameterName);
					elements.add(element);
				};
				
			} else {
				template.changeParameterName(mapping.getSrcParameter(), mapping.getDstParameter());
			}
			
		}
		
		for (int i = 0; i < parameterNames.size(); i++) {
			
			progress.map(srcTemplate, parameterNames.get(i), elements.get(i));
			
		}
	 }
	
	
	// HELPERS
	private void fetch(String srcLangCode, String dstLangCode, Collection<String> srcLinks, HashMap<String, String> linkMap)
	 {
		// redirect
		RedirectsAction redirectAction = new RedirectsAction(srcLinks);
		MinorityTranslateModel.bots().fetchBot(srcLangCode).getPerformedAction(redirectAction);
		ArrayList<String> redirected = new ArrayList<>();
		for (String srcLink : srcLinks) {
			redirected.add(redirectAction.findRedirect(srcLink));
		}

		// langlinks
		EntitiesAction entitiesAction = new EntitiesAction(srcLangCode, redirected, new String[]{dstLangCode});
		MinorityTranslateModel.bots().getWikidataBot().getPerformedAction(entitiesAction);
		
		for (String link : srcLinks) {
			String srcRedirect = redirectAction.findRedirect(link);
			String qid = entitiesAction.findQid(srcRedirect);
			
			String dstLink = entitiesAction.findLangtitle(qid, dstLangCode);
			
			// System.out.println(qid + ":" + link + " -> " + srcRedirect + " -> " + dstLink);
			
			if (dstLink != null) linkMap.put(link, dstLink);
		}
	 }
	
	private HashMap<String, String> retrieveCache(String srcLangCode, String dstLangCode) {
		String langPair = srcLangCode + "|" + dstLangCode;
		HashMap<String, String> cache = linkMapCache.get(langPair);
		if (cache != null) return cache;
		cache = new HashMap<>();
		linkMapCache.put(langPair, cache);
		return cache;
	}

	private boolean startsLowerCase(String word)
	 {
		if (word.isEmpty()) return false;
		return Character.isLowerCase(word.charAt(0));
	 }

	private String lowerFirstCase(String word)
	 {
		if (word.isEmpty()) return word;
		return Character.toLowerCase(word.charAt(0)) + word.substring(1);
	 }
	
}
