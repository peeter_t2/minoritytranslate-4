package ee.translate.keeleleek.mtapplication.view.javafx.dialogs;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.puremvc.java.multicore.interfaces.INotification;

import ee.translate.keeleleek.mtapplication.Notifications;
import ee.translate.keeleleek.mtapplication.model.MinorityTranslateModel;
import ee.translate.keeleleek.mtapplication.model.lists.ListSuggestion;
import ee.translate.keeleleek.mtapplication.model.lists.SuggestionList;
import ee.translate.keeleleek.mtapplication.view.dialogs.ListsDialogMediator;
import ee.translate.keeleleek.mtapplication.view.javafx.fxemelents.SuggestionFX;
import ee.translate.keeleleek.mtapplication.view.javafx.fxemelents.SuggestionListFX;
import ee.translate.keeleleek.mtapplication.view.javafx.fxemelents.SuggestionListControlFX;
import ee.translate.keeleleek.mtapplication.view.javafx.fxemelents.SuggestionListSectionFX;
import ee.translate.keeleleek.mtapplication.view.javafx.fxemelents.ExposableConfirmFX;
import ee.translate.keeleleek.mtapplication.view.javafx.fxemelents.SuggestionControlFX;
import ee.translate.keeleleek.mtapplication.view.javafx.fxemelents.SuggestionSectionFX;
import javafx.beans.InvalidationListener;
import javafx.beans.Observable;
import javafx.beans.value.ObservableValue;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.geometry.Point2D;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBoxTreeItem;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressIndicator;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.SplitPane;
import javafx.scene.control.Tooltip;
import javafx.scene.control.TreeCell;
import javafx.scene.control.TreeItem;
import javafx.scene.control.TreeTableColumn;
import javafx.scene.control.TreeTableView;
import javafx.scene.control.TreeView;
import javafx.scene.control.cell.CheckBoxTreeCell;
import javafx.util.Callback;

public class ListsDialogMediatorFX extends ListsDialogMediator implements ExposableConfirmFX {

	@FXML
	private SplitPane suggestionsSplit;
	
	@FXML
	private TreeView<SuggestionListFX> listsTree;
	@FXML
	private TreeTableView<SuggestionFX> suggestionsTable;
	@FXML
	private TreeTableColumn<SuggestionFX, String> titleColumn;
	
	@FXML
	private ProgressIndicator busyIndicator;
	@FXML
	private Label busyIndicatorLabel;

	private Tooltip listTooltip = null;
	
	@FXML
	private Button removeButton;
	@FXML
	private Button resetButton;
	@FXML
	private Button confirmButton = null;
	
	private TreeItem<SuggestionListFX> listsRootItem = new TreeItem<>(new SuggestionListSectionFX("root"), null);
	private HashMap<String, TreeItem<SuggestionListFX>> listsSubItems = new HashMap<>();
	
	private TreeItem<SuggestionFX> suggestRootItem = new TreeItem<>(new SuggestionSectionFX("root"), null);
	private HashMap<String, TreeItem<SuggestionFX>> suggestSubItems = new HashMap<>();
	
	boolean manual = false;
	
	
	// INIT
	@FXML
	private void initialize()
	 {
		suggestionsTable.setRoot(suggestRootItem);
		suggestionsTable.setShowRoot(false);
		suggestRootItem.setExpanded(true);
		
		listsTree.setRoot(listsRootItem);
		listsTree.setShowRoot(false);
		listsRootItem.setExpanded(true);
		
		listsTree.setCellFactory(new Callback<TreeView<SuggestionListFX>, TreeCell<SuggestionListFX>>() {
            @Override
            public TreeCell<SuggestionListFX> call(TreeView<SuggestionListFX> param) {
                return new CheckBoxTreeCell<SuggestionListFX>() {
                    @Override
                    public void updateItem(SuggestionListFX item, boolean empty){
                        super.updateItem(item, empty);
                        // fully empty
                        if(empty) {
                            setText(null);
                        // checkbox
                        } else if (getTreeItem() instanceof CheckBoxTreeItem) {
                            // default is fine
                        }
                        // everything else
                        else {
                        	setGraphic(null);
                        }
                    }
                };
            }
        });
		
		/*listsTree.setCellFactory(CheckBoxTreeCell.forTreeView(new Callback<TreeItem<SuggestionListFX>, ObservableValue<Boolean>>() {
            @Override
            public ObservableValue<Boolean> call(TreeItem<SuggestionListFX> suggestion) {
            	if (suggestion.getValue() instanceof SuggestionListListingFX) {
            		SuggestionListListingFX suggextionListing = (SuggestionListListingFX) suggestion.getValue();
            		return suggextionListing.enabled();
            	} else {
            		return new SimpleBooleanProperty();
            	}
            }
        }));*/
		
		listsTree.getSelectionModel().selectedItemProperty().addListener(new InvalidationListener() {
			@Override
			public void invalidated(Observable observable) {
				
				if (listTooltip != null) listTooltip.hide();

				listTooltip = new Tooltip(listsTree.getSelectionModel().getSelectedItem().getValue().getFullName());
				listTooltip.setAutoHide(true);
				
				Point2D p = listsTree.localToScene(0.0, 0.0);
				p = p.add(listsTree.getScene().getX(), listsTree.getScene().getY());
				p = p.add(listsTree.getScene().getWindow().getX(), listsTree.getScene().getWindow().getY());
				p = p.add(0.0, 15.0 + listsTree.getHeight());
				
				listTooltip.show(listsTree, p.getX(), p.getY());
				
			}
		});

		// suggestions table
		suggestionsTable.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
		
		// columns
		titleColumn.setCellValueFactory(new Callback<TreeTableColumn.CellDataFeatures<SuggestionFX,String>, ObservableValue<String>>() {
			@Override
			public ObservableValue<String> call(TreeTableColumn.CellDataFeatures<SuggestionFX, String> param) {
				return param.getValue().getValue().title();
			}
		});
		
		// suggestion lists
		List<SuggestionList> usersLists = MinorityTranslateModel.lists().collectSuggestionLists();
		for (SuggestionList sugestList : usersLists) {
			
			final SuggestionListControlFX suggestion = new SuggestionListControlFX(sugestList, MinorityTranslateModel.lists().isSuggestionListEnabled(sugestList.getName()));
			CheckBoxTreeItem<SuggestionListFX> treeItem = new CheckBoxTreeItem<>(suggestion);
			
			fetchGroupItem(sugestList.getGroup()).getChildren().add(treeItem);
			
			treeItem.setSelected(suggestion.enabled().get());
			suggestion.enabled().bind(treeItem.selectedProperty());
			
			suggestion.enabled().addListener(new InvalidationListener() {
				@Override
				public void invalidated(Observable observable) {
					sendNotification(Notifications.SUGGESTION_LIST_ENABLE, suggestion.enabled().get(), suggestion.getFullName());
				}
			});
			
		}
		
		// initial data
		List<ListSuggestion> suggestions = MinorityTranslateModel.lists().collectSuggestions();
		for (ListSuggestion suggestion : suggestions) {
			addSuggestion(suggestion);
		}
		
		// languages
		String[] langCodes = MinorityTranslateModel.preferences().getDstLangCodes();
		for (final String langCode : langCodes) {
			
			TreeTableColumn<SuggestionFX, String> column = new TreeTableColumn<>(langCode);
			column.setMaxWidth(50);
			column.setPrefWidth(50);
			column.setMinWidth(50);
			column.setResizable(false);
			column.getStyleClass().add("centered-column");
			
			suggestionsTable.getColumns().add(column);
			
			column.setCellValueFactory(new Callback<TreeTableColumn.CellDataFeatures<SuggestionFX,String>, ObservableValue<String>>() {
				@Override
				public ObservableValue<String> call(TreeTableColumn.CellDataFeatures<SuggestionFX, String> param) {
					return param.getValue().getValue().translated(langCode);
				}
			});
			
		}
		
		// buttons
		suggestionsTable.getSelectionModel().getSelectedItems().addListener(new InvalidationListener() {
			@Override
			public void invalidated(Observable observable) {
				if (confirmButton != null) confirmButton.setDisable(suggestionsTable.getSelectionModel().getSelectedIndex() == -1);
				removeButton.setDisable(suggestionsTable.getSelectionModel().getSelectedIndex() == -1);
			}
		});

		suggestionsSplit.setDividerPositions(0.15);
		
		// refresh
		refreshReset();
		refreshBusy();
	 }
	
	@Override
	public void exposeConfirm(final Button button) {
		button.setDisable(suggestionsTable.getSelectionModel().isEmpty());
		this.confirmButton = button;
	}

	
	// INIT (FRAMEWORK)
	@Override
	public String[] listNotificationInterests() {
		return new String[]
		 {
			Notifications.SUGGESTION_LIST_ARTICLE_ADDED,
			Notifications.SUGGESTION_LIST_ARTICLE_REMOVED,
			Notifications.SUGGESTION_LIST_CLEAN_CHANGED,
			Notifications.SUGGESTION_LIST_BUSY
		 };
	}
	
	@Override
	public void handleNotification(INotification notification)
	 {
		switch (notification.getName()) {
			
		case Notifications.SUGGESTION_LIST_ARTICLE_ADDED:
			addSuggestion((ListSuggestion) notification.getBody());
			break;

		case Notifications.SUGGESTION_LIST_ARTICLE_REMOVED:
			removeSuggestion((ListSuggestion) notification.getBody());
			break;

		case Notifications.SUGGESTION_LIST_CLEAN_CHANGED:
			refreshReset();
			break;

		case Notifications.SUGGESTION_LIST_BUSY:
			refreshBusy();
			break;
		
		 default:
			break;
		}
	 }

	
	// REFRESH
	private void refreshReset()
	 {
		resetButton.setDisable(MinorityTranslateModel.lists().isClean());
	 }
	
	private void refreshBusy()
	 {
		busyIndicator.setVisible(MinorityTranslateModel.lists().isBusy());
		busyIndicatorLabel.setVisible(busyIndicator.isVisible());
	 }
	
	
	// INHERIT (CONFIRM DIALOG)
	@Override
	public void prepare()
	 {
		super.prepare();
		
	 }
	
	
	@Override
	public void confirm()
	 {
		ObservableList<TreeItem<SuggestionFX>> selected = suggestionsTable.getSelectionModel().getSelectedItems();
		
		ArrayList<SuggestionControlFX> suggestions = new ArrayList<>();
		collectSuggestions(selected, suggestions);
		
		for (SuggestionControlFX suggestionFX : suggestions) {
			getFacade().sendNotification(Notifications.ADD_EXPANDED_CONTENT, suggestionFX.getRequest());
			getFacade().sendNotification(Notifications.SUGGESTION_LIST_REMOVE_SUGGESTION, suggestionFX.getSuggestion());
		}
	 }
	
	
	// INHERIT
	@Override
	protected void setListNames(String[] listNames) {
		manual = true;
		manual = false;
	}
	
	@Override
	protected void setLanguages(String[] languages) {
		manual = true;
		manual = false;
	}
	
	@Override
	public String getListName() {
		return "";
	}

	@Override
	public String getLanguage() {
		return "";
	}

	@Override
	protected void setList(String[] values) {
	}

	@Override
	public String[] getSelected() {
		return new String[]{};
	}

	@Override
	protected void setProgress(double progress) {
	}

	@Override
	protected void setProgressVisible(boolean visible) {
	}

	
	@Override
	protected void unselect() {
	}
	
	
	@Override
	public boolean isListsEnabled() {
		return false;
	}
	
	@Override
	public void setListsEnabled(boolean enabled) {
	}
	
	@Override
	public void addSuggestion(ListSuggestion suggestion) {
		TreeItem<SuggestionFX> subItem = fetchSubItem(suggestion.getTypeList());
		subItem.getChildren().add(new TreeItem<SuggestionFX>(new SuggestionControlFX(suggestion)));
	}
	
	@Override
	public void removeSuggestion(ListSuggestion suggestion) {
	}
	
	
	// EVENTS
	@FXML
	public void onEnabledToggle() {
		super.onEnabledToggle();
	}

	@FXML
	public void onRemoveClick()
	 {
		ObservableList<TreeItem<SuggestionFX>> selected = suggestionsTable.getSelectionModel().getSelectedItems();
		ArrayList<TreeItem<SuggestionFX>> removeItems = new ArrayList<>(selected);
		
		for (TreeItem<SuggestionFX> treeItem : removeItems) {
			removeSuggestionTreeItem(treeItem);
		}
		
		if (selected.size() > 1) { // correct selection
			TreeItem<SuggestionFX> select = selected.get(0);
			suggestionsTable.getSelectionModel().clearSelection();
			suggestionsTable.getSelectionModel().select(select);
		}
		
		if (suggestRootItem.getChildren().size() == 0) suggestionsTable.getSelectionModel().clearSelection();
	 }

	@FXML
	public void onResetClick()
	 {
		suggestRootItem.getChildren().clear();
		suggestSubItems.clear();
		sendNotification(Notifications.SUGGESTION_LIST_RESET);
	 }
	
	
	// HELPERS
	private TreeItem<SuggestionListFX> fetchGroupItem(String group)
	 {
		TreeItem<SuggestionListFX> item = listsSubItems.get(group);
		if (item != null) return item;
		
		// create new item
		item = new TreeItem<SuggestionListFX>(new SuggestionListSectionFX(group));
		listsSubItems.put(group, item);
		listsRootItem.getChildren().add(item);

		return item;
	 }

	
	private TreeItem<SuggestionFX> fetchSubItem(List<String> nameList)
	 {
		if (nameList.isEmpty()) return suggestRootItem;
		
		String name = nameList.toString();
		
		TreeItem<SuggestionFX> item = suggestSubItems.get(name);
		if (item != null) return item;
		
		// create new item
		item = new TreeItem<SuggestionFX>(new SuggestionSectionFX(nameList.get(nameList.size() - 1)));
		if (nameList.size() == 1) item.setExpanded(true);
		suggestSubItems.put(name, item);
		
		// add to parent
		ArrayList<String> parentNameList = new ArrayList<>(nameList);
		parentNameList.remove(nameList.size() - 1);
		TreeItem<SuggestionFX> parentItem = fetchSubItem(parentNameList);
		parentItem.getChildren().add(item);

		return item;
	 }

	private void collectSuggestions(ObservableList<TreeItem<SuggestionFX>> treeItems, List<SuggestionControlFX> suggestionsOut)
	 {
		for (TreeItem<SuggestionFX> treeItem : treeItems) {
			
			// children
			if (treeItem.getChildren().size() > 0) {
				collectSuggestions(treeItem.getChildren(), suggestionsOut);
				continue;
			}
			
			// current item
			SuggestionFX suggestion = treeItem.getValue();
			if (suggestion instanceof SuggestionControlFX) suggestionsOut.add((SuggestionControlFX) suggestion);
			
		}
	 }

	private void removeSuggestionTreeItem(TreeItem<SuggestionFX> treeItem)
	 {
		// children
		ObservableList<TreeItem<SuggestionFX>> children = treeItem.getChildren();
		while (children.size() > 0) {
			removeSuggestionTreeItem(children.get(children.size() - 1));
		}

		// current item
		SuggestionFX suggestion = treeItem.getValue();
		if (suggestion instanceof SuggestionControlFX) {
			SuggestionControlFX suggestionRequest = (SuggestionControlFX) suggestion;
			getFacade().sendNotification(Notifications.SUGGESTION_LIST_REMOVE_SUGGESTION, suggestionRequest.getSuggestion());
		}
		treeItem.getParent().getChildren().remove(treeItem);
	 }
	
	
}
