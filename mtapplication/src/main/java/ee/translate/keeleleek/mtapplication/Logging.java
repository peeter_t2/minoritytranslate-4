package ee.translate.keeleleek.mtapplication;

import java.net.URL;

import org.apache.log4j.PropertyConfigurator;

public class Logging {
	
	public static void setup() {
		URL loggerConfig = Logging.class.getResource("/logging.properties");
		PropertyConfigurator.configure(loggerConfig);
	}
	
}
