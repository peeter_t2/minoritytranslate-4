package ee.translate.keeleleek.mtapplication.view.javafx.fxemelents;

import ee.translate.keeleleek.mtapplication.model.preferences.TemplateMapping;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class TemplateMappingFX {

	public final StringProperty srcTemplate;
	public final StringProperty srcParameter;
	public final StringProperty srcLang;
	public final StringProperty dstParameter;
	public final StringProperty dstLang;
	
	
	public TemplateMappingFX(TemplateMapping mapping)
	 {
		this.srcTemplate = new SimpleStringProperty(mapping.getSrcTemplate());
		this.srcParameter = new SimpleStringProperty(mapping.getSrcParameter());
		this.srcLang = new SimpleStringProperty(mapping.getSrcLang());
		this.dstParameter = new SimpleStringProperty(mapping.getDstParameter());
		this.dstLang = new SimpleStringProperty(mapping.getDstLang());
	 }

	public TemplateMapping toMapping()
	 {
		return new TemplateMapping(srcTemplate.getValue(), srcParameter.getValue(), dstLang.getValue(), dstParameter.getValue(), srcLang.getValue());
	 }
	
	
}
