package ee.translate.keeleleek.mtapplication.view.javafx.helpers;

public class UnicodePage {

	private String name;
	private char start;
	private char end;
	
	
	public UnicodePage(String name, char start) {
		super();
		this.name = name;
		this.start = start;
		this.end = start;
	}
	
	
	public String getName() {
		return name;
	}
	
	public char getStart() {
		return start;
	}
	
	public char getEnd() {
		return end;
	}
	
	public void setEnd(char end) {
		this.end = end;
	}
	
	
}
