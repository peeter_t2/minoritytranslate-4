package ee.translate.keeleleek.mtapplication.model.preferences;

import java.util.ArrayList;

import ee.translate.keeleleek.mtapplication.model.content.MinorityArticle;
import ee.translate.keeleleek.mtapplication.model.processing.RegexReplace;

/**
 * Immutable preferences for replace.
 *
 */
public class ReplacePreferences {

	private ArrayList<ReplaceEntry> replaces = new ArrayList<>();

	
	/* ******************
	 *                  *
	 *  Initialization  *
	 *                  *
	 ****************** */
	public ReplacePreferences()
	 {
		this.replaces = new ArrayList<>();
	 }
	
	public ReplacePreferences(ArrayList<ReplaceEntry> replaces)
	 {
		this.replaces = replaces;
	 }

	public ReplacePreferences(ReplacePreferences other)
	 {
		this.replaces = new ArrayList<>(other.replaces.size());
		for (int i = 0; i < other.replaces.size(); i++) {
			this.replaces.add(new ReplaceEntry(other.replaces.get(i)));
		}
	 }
	
	public static ReplacePreferences create()
	 {
		ReplacePreferences preferences = new ReplacePreferences();
		return preferences;
	 }
	

	
	/* ******************
	 *                  *
	 *   Preferences    *
	 *                  *
	 ****************** */
	public int getReplaceCount() {
		return replaces.size();
	}
	
	public ReplaceEntry getReplace(int i) {
		return replaces.get(i);
	}
	
	public ArrayList<RegexReplace> filterReplaces(MinorityArticle srcArticle, MinorityArticle dstArticle)
	 {
		String srcLangCode = srcArticle.getRef().getLangCode();
		String dstLangCode = dstArticle.getRef().getLangCode();
		
		return filterReplaces(srcLangCode, dstLangCode);
	 }

	public ArrayList<RegexReplace> filterReplaces(String srcLangCode, String dstLangCode)
	 {
		ArrayList<RegexReplace> result = new ArrayList<>();
		for (ReplaceEntry entry : replaces) {
			if (entry.getFilter().isAccept(srcLangCode, dstLangCode)) result.add(entry.createRegex());
		}
		
		return result;
	 }

	
	
	/* ******************
	 *                  *
	 *     Utility      *
	 *                  *
	 ****************** */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((replaces == null) ? 0 : replaces.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ReplacePreferences other = (ReplacePreferences) obj;
		if (replaces == null) {
			if (other.replaces != null)
				return false;
		} else if (!replaces.equals(other.replaces))
			return false;
		return true;
	}
	
	
}
