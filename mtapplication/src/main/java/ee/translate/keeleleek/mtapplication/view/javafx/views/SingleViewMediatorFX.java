package ee.translate.keeleleek.mtapplication.view.javafx.views;

import ee.translate.keeleleek.mtapplication.model.MinorityTranslateModel;
import ee.translate.keeleleek.mtapplication.view.elements.EditorMediator.EditorPosition;
import javafx.fxml.FXML;

public class SingleViewMediatorFX extends ViewMediatorFX {

	
	/* ******************
	 *                  *
	 *  Initialization  *
	 *                  *
	 ****************** */
	public SingleViewMediatorFX(ViewMode viewMode)
	 {
		super(viewMode);
	 }
	
	@FXML
	private void initialize()
	 {
		//super.initialize();
	 }
	
	
	
	/* ******************
	 *                  *
	 *     Refresh      *
	 *                  *
	 ****************** */

	
	
	
	/* ******************
	 *                  *
	 *    Languages     *
	 *                  *
	 ****************** */
	@Override
	public String[] langCodesFor(EditorPosition pos)
	 {
		String[] langCodes;
		if (pos == EditorPosition.DESTINATION) langCodes = MinorityTranslateModel.preferences().getActiveLangCodes();
		else langCodes = new String[0];
		return langCodes;
	 }
	
	
	
	/* ******************
	 *                  *
	 *      Events      *
	 *                  *
	 ****************** */
	@Override
	public String findSelectedSrcLangCode() {
		return null;
	}
	
}
