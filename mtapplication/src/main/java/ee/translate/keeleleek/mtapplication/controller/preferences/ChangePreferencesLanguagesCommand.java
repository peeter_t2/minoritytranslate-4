package ee.translate.keeleleek.mtapplication.controller.preferences;

import org.puremvc.java.multicore.interfaces.INotification;
import org.puremvc.java.multicore.patterns.command.SimpleCommand;

import ee.translate.keeleleek.mtapplication.model.MinorityTranslateModel;
import ee.translate.keeleleek.mtapplication.model.preferences.LanguagesPreferences;

public class ChangePreferencesLanguagesCommand extends SimpleCommand {

	@Override
	public void execute(INotification notification)
	 {
		LanguagesPreferences preferences = (LanguagesPreferences) notification.getBody();
		
		MinorityTranslateModel.preferences().changeLanguages(preferences);
	 }
	
}
