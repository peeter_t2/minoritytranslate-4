package ee.translate.keeleleek.mtapplication.controller.session;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.puremvc.java.multicore.interfaces.INotification;
import org.puremvc.java.multicore.patterns.command.SimpleCommand;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import ee.translate.keeleleek.mtapplication.MinorityTranslate;
import ee.translate.keeleleek.mtapplication.Notifications;
import ee.translate.keeleleek.mtapplication.common.FileUtil;
import ee.translate.keeleleek.mtapplication.model.MinorityTranslateModel;
import ee.translate.keeleleek.mtapplication.model.content.Reference;
import ee.translate.keeleleek.mtapplication.model.content.WikiReference;
import ee.translate.keeleleek.mtapplication.model.serialising.ReferenceSerializer;
import ee.translate.keeleleek.mtapplication.model.serialising.WikiReferenceSerializer;
import ee.translate.keeleleek.mtapplication.model.session.SessionProxy;

public class AutosaveSessionCommand extends SimpleCommand {

	public final static Charset ENCODING = StandardCharsets.UTF_8;
	public final static String AUTOSAVE_FILE_NAME = "autosave.ses";
	
	private Logger LOGGER = LoggerFactory.getLogger(getClass());
	

	@Override
	public void execute(INotification notification)
	 {
		Path path = Paths.get(MinorityTranslate.ROOT_PATH, "autosave.ses");
		
		sendNotification(Notifications.SESSION_SAVING_BUSY, true);
		
		SessionProxy sessionProxy = MinorityTranslateModel.session();
		
		try {
			GsonBuilder builder = new GsonBuilder();
			builder.registerTypeAdapter(Reference.class, new ReferenceSerializer());
			builder.registerTypeAdapter(WikiReference.class, new WikiReferenceSerializer());
			Gson gson = builder.create();
			String json = gson.toJson(sessionProxy);
			FileUtil.write(path, json);
		} catch (IOException e) {
			LOGGER.error("Failed to write session", e);
		} finally {
			sendNotification(Notifications.SESSION_SAVING_BUSY, false);
		}
	 }

}
