package ee.translate.keeleleek.mtapplication.model.preferences;

import ee.translate.keeleleek.mtapplication.model.autocomplete.AutocompleteChoice;

public class Symbol {

	private EntryFilter filter;
	private String name;
	private String trigger;
	private Character symbol;
	private Integer row = -1;
	
	
	public Symbol(EntryFilter filter, String name, String trigger, Character symbol, Integer row) {
		this.filter = filter;
		this.name = name;
		this.trigger = trigger;
		this.symbol = symbol;
		this.row = row;
	}
	
	public Symbol(String langCode, String name, String trigger, Character symbol, Integer row) {
		this.filter = new EntryFilter(null, langCode);
		this.name = name;
		this.trigger = trigger;
		this.symbol = symbol;
		this.row = row;
	}

	public Symbol(String langCode, String name, String trigger, Character symbol) {
		this.filter = new EntryFilter(null, langCode);
		this.name = name;
		this.trigger = trigger;
		this.symbol = symbol;
		this.row = 0;
	}

	public Symbol(String name, String trigger, Character symbol) {
		this.filter = new EntryFilter(null, null);
		this.name = name;
		this.trigger = trigger;
		this.symbol = symbol;
		this.row = 0;
	}

	public Symbol(Symbol other) {
		this.filter = other.filter;
		this.name = other.name;
		this.trigger = other.trigger;
		this.symbol = other.symbol;
		this.row = other.row;
	}

	
	public AutocompleteChoice toChoice(String prefix, String langCode)
	 {
		if (!prefix.endsWith(trigger) || trigger.isEmpty() || !filter.isAccept("", langCode)) return null;
		return new AutocompleteChoice(symbol.toString(), symbol.toString(), name, trigger.length(), 0);
	 }
	
	
	public EntryFilter getFilter() {
		return filter;
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getTrigger() {
		return trigger;
	}

	public void setTrigger(String trigger) {
		this.trigger = trigger;
	}

	public Character getSymbol() {
		return symbol;
	}

	public void setSymbol(Character symbol) {
		this.symbol = symbol;
	}

	public Integer getRow() {
		return row;
	}
	
	public void setRow(Integer row) {
		this.row = row;
	}


	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((filter == null) ? 0 : filter.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + ((row == null) ? 0 : row.hashCode());
		result = prime * result + ((symbol == null) ? 0 : symbol.hashCode());
		result = prime * result + ((trigger == null) ? 0 : trigger.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Symbol other = (Symbol) obj;
		if (filter == null) {
			if (other.filter != null)
				return false;
		} else if (!filter.equals(other.filter))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (row == null) {
			if (other.row != null)
				return false;
		} else if (!row.equals(other.row))
			return false;
		if (symbol == null) {
			if (other.symbol != null)
				return false;
		} else if (!symbol.equals(other.symbol))
			return false;
		if (trigger == null) {
			if (other.trigger != null)
				return false;
		} else if (!trigger.equals(other.trigger))
			return false;
		return true;
	}

	
}
