package ee.translate.keeleleek.mtapplication.model.preferences;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;

import org.puremvc.java.multicore.patterns.proxy.Proxy;

import ee.translate.keeleleek.mtapplication.Notifications;
import ee.translate.keeleleek.mtapplication.model.autocomplete.AutocompleteChoice;
import ee.translate.keeleleek.mtapplication.model.content.TextAligner;
import ee.translate.keeleleek.mtapplication.model.filters.FilesFilter;
import ee.translate.keeleleek.mtapplication.model.filters.Filter;
import ee.translate.keeleleek.mtapplication.model.filters.IntroductionFilter;
import ee.translate.keeleleek.mtapplication.model.filters.ReferencesFilter;
import ee.translate.keeleleek.mtapplication.model.filters.TemplatesFilter;
import ee.translate.keeleleek.mtapplication.model.processing.processers.VariableProcesser;

public class PreferencesProxy extends Proxy {

	transient public final static String NAME = "{C4D99F28-356F-4855-A7D8-310A5CC9E1E3}";
	
	private CredentialsPreferences credentials = new CredentialsPreferences();
	private ProgramPreferences program = new ProgramPreferences();
	private LanguagesPreferences languages = new LanguagesPreferences();
	private CorpusPreferences corpus = new CorpusPreferences();
	private CollectPreferences collects = null;
	private ReplacePreferences replaces = null;
	private SnippetsPreferences snippets= new SnippetsPreferences();
	private VisualPreferences visual;
	private InsertsPreferences inserts = null;
	private ContentAssistPreferences contentAssist = new ContentAssistPreferences();
	private TemplateMappingPreferences templateMapping = new TemplateMappingPreferences();
	private SymbolsPreferences symbols = new SymbolsPreferences();
	private LookupsPreferences lookups = null;

	private HashSet<String> acknowledged = new HashSet<>();
	
	private Boolean listsEnabled = false;
	private Boolean fresh = true;
	
	
	/* ******************
	 *                  *
	 *  Initialization  *
	 *                  *
	 ****************** */
	public PreferencesProxy() {
		super(NAME, "");
	}
	
	@Override
	public void onRegister()
	 {
		if (visual == null) visual = VisualPreferences.create();
		if (inserts == null) inserts = InsertsPreferences.create();
		if (lookups == null) lookups = LookupsPreferences.create();
		if (collects == null) collects = CollectPreferences.create();
		if (replaces == null) replaces = ReplacePreferences.create();
	 }

	

	/* ******************
	 *                  *
	 *     Changing     *
	 *                  *
	 ****************** */
	public void changePreferences(Object preferences)
	 {
		if (preferences instanceof CollectPreferences) {
			if (this.collects.equals(preferences)) return;
			this.collects = (CollectPreferences) preferences;
		}
		
		if (preferences instanceof ReplacePreferences) {
			if (this.replaces.equals(preferences)) return;
			this.replaces = (ReplacePreferences) preferences;
		}
		
		
	 }
	
	
	
	// CREDENTIALS
	public CredentialsPreferences credentials()
	 {
		return credentials;
	 }
	
	public void changeCredentials(CredentialsPreferences credentials)
	 {
		if (this.credentials.equals(credentials)) return;
		
		this.credentials = credentials;
	 }

	
	// PROGRAM
	public ProgramPreferences program()
	 {
		return program;
	 }
	
	public void changeProgram(ProgramPreferences program)
	 {
		if (this.program.equals(program)) return;
		
		this.program = program;

		sendNotification(Notifications.PREFERENCES_PROGRAM_CHANGED);
		sendNotification(Notifications.PREFERENCES_FILTERS_CHANGED);
	 }

	public Filter[] getActiveFilters()
	 {
		return program.collectActiveFilters();
	 }

	public void changeFilter(String filterName, boolean enabled)
	 {
		switch (filterName) {
		case TemplatesFilter.NAME:
			program.setTemplatesFilter(enabled);
			break;

		case FilesFilter.NAME:
			program.setFilesFilter(enabled);
			break;

		case IntroductionFilter.NAME:
			program.setIntroductionFilter(enabled);
			break;

		case ReferencesFilter.NAME:
			program.setReferencesFilter(enabled);
			break;

		default:
			break;
		}
		
		sendNotification(Notifications.PREFERENCES_FILTERS_CHANGED);
	 }

	public String getGUILangCode() {
		return program.getGUILangCode();
	}
	
	public String getFontSize() {
		return program.getFontSize();
	}
	
	
	// LANGUAGES
	public LanguagesPreferences languages()
	 {
		return languages;
	 }
	
	public void changeLanguages(LanguagesPreferences languages)
	 {
		if (this.languages.equals(languages)) return;
		
		this.languages = languages;

		sendNotification(Notifications.PREFERENCES_LANGUAGES_CHANGED);
	 }
	

	public String[] getLangCodes() {
		return languages.getLangCodes().toArray(new String[languages.getLangCodes().size()]);
	}
	
	public ArrayList<String> getAllLangCodes() {
		return languages.getLangCodes();
	}
	

	public String[] getDstLangCodes()
	 {
		return languages.filterDstLangCodes();
	 }

	public String getLastDstLangCode()
	 {
		String[] langCodes = languages.filterDstLangCodes();
		if (langCodes.length == 0) return null;
		return langCodes[langCodes.length - 1];
	 }

	public String[] getSrcLangCodes()
	 {
		return languages.filterSrcLangCodes();
	 }

	public String[] getActiveLangCodes()
	 {
		return languages.filterActiveLangCodes();
	 }
	
	public String getLastSrcLangCode()
	 {
		String[] langCodes = languages.filterSrcLangCodes();
		if (langCodes.length == 0) return null;
		return langCodes[langCodes.length - 1];
	 }
	
	
	// CORPUS
	public CorpusPreferences corpus()
	 {
		return corpus;
	 }
	
	public void changeCorpus(CorpusPreferences corpus)
	 {
		if (this.corpus.equals(corpus)) return;
		
		this.corpus = corpus;

//		sendNotification(Notifications.PREFERENCES_Corpus_CHANGED);
	 }

	
	// VISUAL
	public VisualPreferences getVisual()
	 {
		return new VisualPreferences(visual);
	 }
	
	public void changeVisual(VisualPreferences preferences)
	 {
		if (this.visual.equals(preferences)) return;
		
		this.visual = preferences;

		System.out.println("TO " + this.visual);
		
//		sendNotification(Notifications.PREFERENCES_PERSONALISATION_CHANGED);
	 }

	public int getAddonsDivider() {
		return visual.getAddonsDivider();
	}

	public double getTranslateDivider() {
		return visual.getTranslateDivider();
	}

	
	// PROCESSING
	public TemplateMappingPreferences getTemplateMapping() {
		return templateMapping;
	}
	
	public void changeTemplateMapping(TemplateMappingPreferences templateMapping)
	 {
		if (this.templateMapping.equals(templateMapping)) return;
		this.templateMapping = templateMapping;
	 }
	

	// COLLECTS
	public CollectPreferences collects() {
		return collects;
	}
	
	// REPLACES
	public ReplacePreferences replaces() {
		return replaces;
	}
	
	
	// CONTENT ASSIST
	public ContentAssistPreferences getContentAssist() {
		return new ContentAssistPreferences(contentAssist);
	}
	
	public void changeContentAssist(ContentAssistPreferences contentAssist)
	 {
		if (this.contentAssist.equals(contentAssist)) return;
		this.contentAssist = contentAssist;
	 }
	
	public boolean isInsertAuto() {
		return contentAssist.getInsertSingleAuto();
	}

	public boolean isSearchLinks() {
		return contentAssist.getSearchLinks();
	}

	public boolean isTranslateWikilinks() {
		return contentAssist.getTranslateWikilinks();
	}

	public boolean isTranslateTemplates() {
		return contentAssist.getTranslateTemplates();
	}
	

	// INSERTS
	public InsertsPreferences getInserts() {
		return new InsertsPreferences(inserts);
	}
	
	public void changeInserts(InsertsPreferences inserts)
	 {
		if (this.inserts.equals(inserts)) return;
		this.inserts = inserts;
	 }
	
	public List<AutocompleteChoice> findAutocompletes(String prefix, String langCode)
	 {
		ArrayList<AutocompleteChoice> result = new ArrayList<>();
		
		// templates
		ArrayList<Insert> inserts = this.inserts.getInserts();
		for (Insert insert : inserts) {
			AutocompleteChoice choice = insert.toChoice(prefix, langCode);
			if (choice != null) result.add(choice);
		}
		
		// symbols
		ArrayList<Symbol> symbols = this.symbols.getSymbols();
		for (Symbol symbol : symbols) {
			AutocompleteChoice choice = symbol.toChoice(prefix, langCode);
			if (choice != null) result.add(choice);
		}
		
		return result;
	 }
	
	
	// SYMBOLS
	public SymbolsPreferences getSymbols()
	 {
		return new SymbolsPreferences(symbols);
	 }
	
	public void changeSymbols(SymbolsPreferences symbols)
	 {
		if (this.symbols.equals(symbols)) return;
		
		this.symbols = symbols;

		sendNotification(Notifications.PREFERENCES_SYMBOLS_CHANGED);
	 }

	
	// LOOKUPS
	public LookupsPreferences getLookups() {
		return lookups;
	}
	
	public void changeLookups(LookupsPreferences lookups)
	 {
		if (this.lookups.equals(lookups)) return;
		
		this.lookups = lookups;
		
		sendNotification(Notifications.PREFERENCES_LOOKUPS_CHANGED);
	 }
	
	
	// SNIPPETS
	public String getSnippet(int i)
	 {
		return snippets.getSnippet(i);
	 }
	
	public String findSnippet(int i,String langCode)
	 {
		return VariableProcesser.process(langCode, snippets.getSnippet(i));
	 }
	
	public void chnageSnippet(int i, String snippet)
	 {
		snippets.setSnippet(i, snippet);
	 }
	
	
	// LISTS
	public boolean isListsEnabled() {
		return listsEnabled;
	}
	
	public void updateListsEnabled(boolean enabled) {
		listsEnabled = enabled;
		sendNotification(Notifications.PREFERENCES_LISTS_ENABLED_CHANGED);
	}

	
	// ALIGNER
	public TextAligner getTextAligner() {
		return new TextAligner();
	}
	
	
	// FRESH
	public Boolean isFresh() {
		return fresh;
	}
	
	public void setFresh(Boolean fresh) {
		this.fresh = fresh;
	}
	
	
	// ACKNOWLEDGE
	public void acknowledgeQuickStart(String name) {
		acknowledged.add(name);
	}

	public boolean isQuickStartAckowledged(String name) {
		return acknowledged.contains(name);
	}

	public void resetQuickStart() {
		acknowledged.clear();;
	}
	
	
	// TYPES
	public enum Proficiency { UNSPECIFIED, LEVEL1, LEVEL2, LEVEL3, LEVEL4, LEVEL5 }
	
	public enum Display {
	    SOURCE, DESTINATION, NONE
	}

	public static class Languages {
		public String[] langCodes;
		public HashMap<String, Proficiency> proficiencies;
		public HashMap<String, Display> displays;
	}

	
}
