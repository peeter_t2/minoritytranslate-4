package ee.translate.keeleleek.mtapplication.model.content;

import java.util.ArrayList;
import java.util.Queue;

import org.puremvc.java.multicore.patterns.proxy.Proxy;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ee.translate.keeleleek.mtapplication.Notifications;
import ee.translate.keeleleek.mtapplication.model.MinorityTranslateModel;
import ee.translate.keeleleek.mtapplication.model.content.MinorityArticle.Status;
import net.sourceforge.jwbf.core.contentRep.Article;
import net.sourceforge.jwbf.mediawiki.bots.MediaWikiBot;

public class ContentDownloaderProxy extends Proxy implements Runnable {

	public final static String NAME = "{AF3C56EA-BC6C-4805-ACFA-C31A804D94F6}";
	
	private static Logger LOGGER = LoggerFactory.getLogger(ContentDownloaderProxy.class);
	
	public final static long TIMEOUT = 250*0 + 1000;
	public final static int MAX_DOWNLOADS = 10;
	
	
	private ArrayList<FullRequest> downloading = new ArrayList<>();

	private boolean busy = false;
	private int guard = 0;
	private boolean run = false;

	
	// INIT
	public ContentDownloaderProxy() {
		super(NAME, "");
	}

	@Override
	public void onRegister() {
		run = true;
		
		startThread();
	}
	
	@Override
	public void onRemove() {
		run = false;
	}

	private void startThread()
	 {
		Thread thread = new Thread(this);
		thread.setDaemon(true);
		thread.start();
	 }
	
	
	// WORK
	@Override
	public void run()
	 {
		try {
			while (run) {
				work();
				Thread.sleep(TIMEOUT);
			}
		}
		catch (InterruptedException e) {
			LOGGER.error("Thread sleep failure!", e);
		}
		catch (IllegalStateException e) {
			LOGGER.warn("Failed to download content: " + e.getMessage() + "!");
			startThread();
		}
	 }
	
	private void work()
	 {
		lookBusy();
		
		if (downloading.size() >= MAX_DOWNLOADS) return;
		
		Queue<MinorityArticle> articles = ContentUtil.filterQidSortArticles(MinorityTranslateModel.content(), Status.DOWNLOAD_REQUESTED);
		
		while (!articles.isEmpty() && downloading.size() < MAX_DOWNLOADS) {
			
			MinorityArticle article = articles.remove();
			download(article.createFullRef());
        
		}
		
		lookBusy();
	 }

	private void lookBusy()
	 {
		boolean busy = downloading.size() > 0;
		
		if (this.busy == busy) return;
		
		this.busy = busy;
		
		MinorityTranslateModel.notifier().sendNotificationThreadSafe(Notifications.ENGINE_DOWNLOADER_BUSY, busy);
	 }
	
	
	// DOWNLOADING
	private void download(final FullRequest fullRef)
	 {
		Thread thread = new Thread(new Runnable() {
			
			@Override
			public void run()
			 {
				if (downloading.contains(fullRef)) return;
				int dguard = guard;
				
				final Reference ref = fullRef.createReference();
				
				MinorityTranslateModel.notifier().sendNotificationThreadSafe(Notifications.ENGINE_DOWNLOAD_STARTED, ref);
				MinorityTranslateModel.content().changeStatus(ref, Status.DOWNLOAD_IN_PROGRESS);
				downloading.add(fullRef);
				
				String langCode = fullRef.getLangCode();
				String title = fullRef.getWikiTitle();
				
				String text = "";
				String rev = "";
				if (!title.isEmpty()) {
					MediaWikiBot bot = MinorityTranslateModel.bots().fetchBot(langCode);
					Article wikiArticle = bot.getArticle(title);
					text = wikiArticle.getText();
					title = wikiArticle.getTitle();
					rev = wikiArticle.getRevisionId();
				}
				
				downloading.remove(fullRef);
				MinorityTranslateModel.notifier().sendNotificationThreadSafe(Notifications.ENGINE_DOWNLOAD_ENDED, ref);
				
				if (dguard == guard) MinorityTranslateModel.content().initDownload(ref, rev, title, text);
				
				LOGGER.info("Downloading complete for article with reference " + ref);
			 }
		});
		
		LOGGER.info("Requesting download for " + fullRef);
		thread.start();
		
	 }
	
	
	// CANCEL
	public void cancel() {
		downloading.clear();
		this.guard++;
	}
	
}
