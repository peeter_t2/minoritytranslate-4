package ee.translate.keeleleek.mtapplication.controller.preferences;

import org.puremvc.java.multicore.interfaces.INotification;
import org.puremvc.java.multicore.patterns.command.SimpleCommand;

import ee.translate.keeleleek.mtapplication.model.MinorityTranslateModel;
import ee.translate.keeleleek.mtapplication.model.preferences.SymbolsPreferences;

public class ChangePreferencesSymbolsCommand extends SimpleCommand {

	@Override
	public void execute(INotification notification)
	 {
		SymbolsPreferences preferences = (SymbolsPreferences) notification.getBody();
		
		MinorityTranslateModel.preferences().changeSymbols(preferences);
	 }
	
}
