package ee.translate.keeleleek.mtapplication.model.connection;

import java.net.URL;
import java.net.URLConnection;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import org.puremvc.java.multicore.patterns.proxy.Proxy;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ee.translate.keeleleek.mtapplication.Notifications;
import ee.translate.keeleleek.mtapplication.model.MinorityTranslateModel;

public class ConnectionProxy extends Proxy implements Runnable {

	public final static String NAME = "{A7012055-94AB-47EF-9753-05F4933B4EC9}";
	
	
	private static Logger LOGGER = LoggerFactory.getLogger(ConnectionProxy.class);

	private static int CONNECT_TIMEOUT = 2000;
	
	private ScheduledExecutorService scheduler = Executors.newScheduledThreadPool(1);
	
	private boolean first = true;
	private boolean status = false;
	
	
	// INITIATION:
	public ConnectionProxy() {
		super(NAME);
	}

	public void start()
	 {
		scheduler.scheduleAtFixedRate(this, 0, 2, TimeUnit.SECONDS);
	 }
	
	public void stop() {
		scheduler.shutdown();
	}
	
	
	// CONNECTION:
	/* 
	 * Checks for connection.
	 */
	@Override
	public void run()
	 {
		// Probe:
		boolean previous = status;
		status = probeConnection();
		
		// Changed:
		if (previous != status) {
			if (status) LOGGER.info("Connection available");
			else LOGGER.info("Connection unavailable");
			MinorityTranslateModel.notifier().sendNotificationThreadSafe(Notifications.CONNECTION_STATUS_CHANGED, status);
		}
		
		// First time available:
		if (first && status) {
			MinorityTranslateModel.notifier().sendNotificationThreadSafe(Notifications.CONNECTION_INITIAL_AVAILABLE);
			first = false;
		}
	 }

	/**
	 * Checks if there is a connection.
	 * 
	 * @return true if there is a connection
	 */
	public boolean hasConnection() {
		return status;
	}
	
	
	// CHECKS:
	/**
	 * Probes connection.
	 * 
	 * @return
	 */
	public static boolean probeConnection(){
		return probeURL("https://www.wikidata.org");
	  }
	
	/**
	 * Probes an URL.
	 * 
	 * @param urlName url
	 * @return true if connection available
	 */
	public static boolean probeURL(String urlName)
	 {
		try {
			URL url = new URL(urlName);
			URLConnection conn = url.openConnection();  
			conn.setConnectTimeout(CONNECT_TIMEOUT);  
			conn.setReadTimeout(CONNECT_TIMEOUT);  
			conn.getInputStream();
			return true;
		}
		catch (Exception e) {
			return false;
		}
	 }

	
}
