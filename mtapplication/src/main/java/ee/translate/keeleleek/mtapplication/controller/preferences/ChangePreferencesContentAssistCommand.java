package ee.translate.keeleleek.mtapplication.controller.preferences;

import org.puremvc.java.multicore.interfaces.INotification;
import org.puremvc.java.multicore.patterns.command.SimpleCommand;

import ee.translate.keeleleek.mtapplication.model.MinorityTranslateModel;
import ee.translate.keeleleek.mtapplication.model.preferences.ContentAssistPreferences;

public class ChangePreferencesContentAssistCommand extends SimpleCommand {

	@Override
	public void execute(INotification notification)
	 {
		ContentAssistPreferences preferences = (ContentAssistPreferences) notification.getBody();
		MinorityTranslateModel.preferences().changeContentAssist(preferences);
	 }
	
}
