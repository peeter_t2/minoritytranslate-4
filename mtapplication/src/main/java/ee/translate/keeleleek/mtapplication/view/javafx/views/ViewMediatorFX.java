package ee.translate.keeleleek.mtapplication.view.javafx.views;

import java.util.ArrayList;
import java.util.List;

import ee.translate.keeleleek.mtapplication.view.elements.EditorMediator;
import ee.translate.keeleleek.mtapplication.view.elements.EditorMediator.EditorMode;
import ee.translate.keeleleek.mtapplication.view.elements.EditorMediator.EditorPosition;
import ee.translate.keeleleek.mtapplication.view.javafx.MediatorLoaderFX;
import ee.translate.keeleleek.mtapplication.view.javafx.editors.AlignEditorMediatorFX;
import ee.translate.keeleleek.mtapplication.view.javafx.editors.EditorMediatorFX;
import ee.translate.keeleleek.mtapplication.view.javafx.editors.SingleEditorMediatorFX;
import ee.translate.keeleleek.mtapplication.view.views.ViewMediator;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;

public class ViewMediatorFX extends ViewMediator {

	// tabs
	@FXML
	protected TabPane upperTabPane;
	@FXML
	protected TabPane lowerTabPane;
	
	// editors
	protected ArrayList<EditorMediator> upperEditors = new ArrayList<>();
	protected ArrayList<EditorMediator> lowerEditors = new ArrayList<>();


	/* ******************
	 *                  *
	 *  Initialization  *
	 *                  *
	 ****************** */
	public ViewMediatorFX(ViewMode viewMode)
	 {
		super(viewMode);
	 }
	
	/* ******************
	 *                  *
	 *    Languages     *
	 *                  *
	 ****************** */
	@Override
	public String findSelectedSrcLangCode() {
		Tab tab = lowerTabPane.getSelectionModel().getSelectedItem();
		if (tab == null) return null;
		String text = tab.getText();
		int i, j;
		i = text.indexOf('(');
		j = text.indexOf(')');
		if (i > 0 && j > i) return text.substring(i + 1, j);
		return text;
	}
	
	
	
	/* ******************
	 *                  *
	 *     Editors      *
	 *                  *
	 ****************** */
	@Override
	public EditorMediator createEditor(EditorMode mode, EditorPosition pos)
	 {
		// create
		EditorMediator editorMediator;
		
		switch (getViewMode()) {
		case SPLIT_HORIZONTAL:
			editorMediator = new EditorMediatorFX(mode);
			MediatorLoaderFX.loadHorizontalEditorMediator(editorMediator);
			break;
			
		case SPLIT_VERTICAL:
			editorMediator = new EditorMediatorFX(mode);
			MediatorLoaderFX.loadVerticalEditorMediator(editorMediator);
			break;

		case ALIGN:
			editorMediator = new AlignEditorMediatorFX(mode);
			MediatorLoaderFX.loadAlignEditorMediator(editorMediator);
			break;

		case SINGLE:
			editorMediator = new SingleEditorMediatorFX(mode);
			MediatorLoaderFX.loadSingleEditorMediator(editorMediator);
			break;
		
		default:
			return null;
			
		}
		
		List<EditorMediator> tabs = getEditors(pos);
		tabs.add(editorMediator);
		
		// add to view
		Tab tab = new Tab();
		tab.setContent((Node) editorMediator.getViewComponent());
		getTabPane(pos).getTabs().add(tab);
		
		return editorMediator;
	 }
	
	@Override
	public EditorMediator destroyEditor(EditorPosition pos)
	 {
		// remove
		List<EditorMediator> tabs = getEditors(pos);
		EditorMediator tabMediator = tabs.remove(tabs.size() - 1);
		
		// remove from gui
		getTabPane(pos).getTabs().remove(tabs.size());
		
		return tabMediator;
	 }
	
	@Override
	public EditorMediator getEditor(EditorPosition pos, int i)
	 {
		return getEditors(pos).get(i);
	 }
	
	@Override
	public int getEditorCount(EditorPosition pos)
	 {
		return getEditors(pos).size();
	 }
	
	@Override
	public List<EditorMediator> getEditors(EditorPosition pos)
	 {
		if (pos == EditorPosition.DESTINATION) return upperEditors;
		else if (pos == EditorPosition.SOURCE) return lowerEditors;
		else return new ArrayList<>();
	 }

	@Override
	public EditorMediator findSelectedEditor(EditorPosition box)
	 {
		int i = getTabPane(box).getSelectionModel().getSelectedIndex();
		List<EditorMediator> editors = getEditors(box);
		if (i < 0 || i >= editors.size()) return null;
		return editors.get(i);
	 }
	
	@Override
	public void commitEditors()
	 {
		int numTabs;

		numTabs = getEditorCount(EditorPosition.DESTINATION);
		for (int i = 0; i < numTabs; i++) {
			getEditor(EditorPosition.DESTINATION, i).commit();
		}

		numTabs = getEditorCount(EditorPosition.SOURCE);
		for (int i = 0; i < numTabs; i++) {
			getEditor(EditorPosition.SOURCE, i).commit();
		}
	 }

	@Override
	public void setEditorName(EditorPosition box, int i, String name)
	 {
		switch (box) {
		case SOURCE:
			lowerTabPane.getTabs().get(i).setText(name);
			break;

		case DESTINATION:
			upperTabPane.getTabs().get(i).setText(name);
			break;

		}
	 }

	@Override
	public void markEditorNew(EditorPosition box, int i, boolean isNew)
	 {
		switch (box) {
		case SOURCE:
			markTabNew(lowerTabPane.getTabs().get(i), isNew);
			break;

		case DESTINATION:
			markTabNew(upperTabPane.getTabs().get(i), isNew);
			break;

		}
	 }
	
	public void markTabNew(Tab tab, boolean isNew)
	 {
		if (isNew) {
			tab.getStyleClass().add("new-article");
			tab.getStyleClass().removeAll("existing-article");
		} else {
			tab.getStyleClass().removeAll("new-article");
			tab.getStyleClass().add("existing-article");
		}
	 }
	
	
	/* ******************
	 *                  *
	 *       GUI        *
	 *                  *
	 ****************** */
	private TabPane getTabPane(EditorPosition pos)
	 {
		if (pos == EditorPosition.DESTINATION) return upperTabPane;
		else if (pos == EditorPosition.SOURCE) return lowerTabPane;
		else return null;
	 }
	
	
	
}
