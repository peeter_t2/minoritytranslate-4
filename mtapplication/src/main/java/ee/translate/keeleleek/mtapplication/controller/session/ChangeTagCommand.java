package ee.translate.keeleleek.mtapplication.controller.session;

import org.puremvc.java.multicore.interfaces.INotification;
import org.puremvc.java.multicore.patterns.command.SimpleCommand;

import ee.translate.keeleleek.mtapplication.model.MinorityTranslateModel;

public class ChangeTagCommand extends SimpleCommand {

	@Override
	public void execute(INotification notification)
	 {
		String tag = (String) notification.getBody();
		MinorityTranslateModel.session().changeTag(tag);
	 }
	
}
