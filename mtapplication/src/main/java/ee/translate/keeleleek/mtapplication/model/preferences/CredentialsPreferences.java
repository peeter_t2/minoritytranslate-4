package ee.translate.keeleleek.mtapplication.model.preferences;


public class CredentialsPreferences {

	private String username;
	private String password;
	
	
	// INIT
	public CredentialsPreferences()
	 {
		this.username = "";
		this.password = "";
	 }
	
	public CredentialsPreferences(String username, String password)
	 {
		this.username = username;
		this.password = password;
	 }


	// PREFERENCES
	public String getUsername() {
		return username;
	}
	
	public String getPassword() {
		return password;
	}
	
	
}
