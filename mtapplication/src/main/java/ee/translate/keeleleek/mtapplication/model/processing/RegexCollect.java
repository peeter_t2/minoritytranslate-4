package ee.translate.keeleleek.mtapplication.model.processing;

import java.util.HashMap;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class RegexCollect {

	protected String rgxFind;
	protected List<String> findParams;
	
	// INIT
	public RegexCollect(String rgxFind, List<String> findParams)
	 {
		this.rgxFind = rgxFind;
		this.findParams = findParams;
	 }
	

	// COLLECT
	public void collect(String text, HashMap<String, String> paramMap)
	 {
		Matcher mFind = Pattern.compile(rgxFind, Pattern.MULTILINE | Pattern.DOTALL).matcher(text);
		
		// collecting parameter
		if (mFind.find()) {
			
			for (String findParam : findParams) {
				
				paramMap.put(findParam, mFind.group(findParam));
				
			}
			
		}
	 }
	
}
