package ee.translate.keeleleek.mtapplication.common.requests;

import java.nio.file.Path;
import java.util.List;

import ee.translate.keeleleek.mtapplication.model.preferences.Symbol;

public class SymbolsImportRequest {

	private String target;
	private Path path;
	
	
	public SymbolsImportRequest(String target, Path path) {
		super();
		this.target = target;
		this.path = path;
	}
	
	
	public SymbolsImportResponse fulfill(List<Symbol> symbols)
	 {
		return new SymbolsImportResponse(target, symbols);
	 }
	
	
	public String getTarget() {
		return target;
	}
	
	public Path getPath() {
		return path;
	}
	
	
	public static class SymbolsImportResponse {
		
		private String target;
		private List<Symbol> symbols;
		
		
		public SymbolsImportResponse(String target, List<Symbol> symbols) {
			super();
			this.target = target;
			this.symbols = symbols;
		}
		
		
		public String getTarget() {
			return target;
		}
		
		public List<Symbol> getSymbols() {
			return symbols;
		}
		
	}
	
}
