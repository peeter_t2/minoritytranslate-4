package ee.translate.keeleleek.mtapplication.model.content;

public enum Namespace {
	
	ARTICLE(""),
	TALK("Talk"),
	USER("User"),
	USER_TALK("User_talk"),
	WIKIPEDIA("Wikipedia"),
	WIKIPEDIA_TALK("Wikipedia_talk"),
	FILE("File"),
	FILE_TALK("File_talk"),
	MEDIAWIKI("MediaWiki"),
	MEDIAWIKI_TALK("MediaWiki_talk"),
	TEMPLATE("Template"),
	TEMPLATE_TALK("Template_talk"),
	HELP("Help"),
	HELP_TALK("Help_talk"),
	CATEGORY("Category"),
	CATEGORY_TALK("Category_talk");
	
	private String wikiName;
	
	
	private Namespace(String wikiName) {
		this.wikiName = wikiName;
	}
	

	public String getWikiName() {
		return wikiName;
	}
	
	public String getVariableName() {
		return "${" + wikiName.replace('_', '-') + "}";
	}
	
	public String getPrefix() {
		if (wikiName.isEmpty()) return "";
		return wikiName + ":";
	}
	
	
	public static Namespace find(int namespace) {
		if (namespace < 0 || namespace >= values().length) return null;
		return values()[namespace];
	}


	public boolean isEmpty() {
		return wikiName.isEmpty();
	}
	
	
}
