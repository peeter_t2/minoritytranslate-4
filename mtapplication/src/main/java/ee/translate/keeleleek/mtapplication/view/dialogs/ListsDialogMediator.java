package ee.translate.keeleleek.mtapplication.view.dialogs;

import org.puremvc.java.multicore.patterns.mediator.Mediator;

import ee.translate.keeleleek.mtapplication.Notifications;
import ee.translate.keeleleek.mtapplication.model.MinorityTranslateModel;
import ee.translate.keeleleek.mtapplication.model.lists.ListSuggestion;
import ee.translate.keeleleek.mtapplication.view.messages.Messages;


public abstract class ListsDialogMediator extends Mediator implements ConfirmDeclineDialog {

	public final static String NAME = "{5B257B79-1C82-4BF3-9035-06F475E01A56}";
	
	
	boolean langName = true;
	
	
	// INIT
	public ListsDialogMediator() {
		super(NAME, null);
	}

	@Override
	public void onRegister()
	 {
	 }
	

	
	// REFRESH
	public abstract String getListName();
	public abstract String getLanguage();
	
	protected abstract void setListNames(String[] listNames);
	protected abstract void setLanguages(String[] languages);
	
	protected abstract void setList(String[] values);
	public abstract String[] getSelected();
	
	protected abstract void setProgress(double progress);
	protected abstract void setProgressVisible(boolean visible);
	
	protected abstract void unselect();

	public abstract boolean isListsEnabled();
	public abstract void setListsEnabled(boolean enabled);

	public abstract void addSuggestion(ListSuggestion suggestion);
	public abstract void removeSuggestion(ListSuggestion suggestion);
	
	
	// INHERIT (CONFIRM DECLINE DIALOG)
	@Override
	public void prepare() {
		
	}

	@Override
	public void focus() {
		
	}
	
	@Override
	public void confirm()
	 {
		if (getSelected().length == 0) {
			sendNotification(Notifications.ERROR_MESSAGE, Messages.getString("messages.list.no.items.selected"), Messages.getString("messages.list.no.items.selected.select"));
			return;
		}
		
		sendNotification(Notifications.MENU_LISTS_CONFIRMED);
		unselect();
	 }
	
	@Override
	public void decline()
	 {
		unselect();
	 }
	
	
	// EVENTS
	protected void onSelectionChange() {
	}

	public void onEnabledToggle() {
		if (isListsEnabled() != MinorityTranslateModel.preferences().isListsEnabled()) sendNotification(Notifications.PREFERENCES_CHANGE_LISTS_ENABLED, isListsEnabled());
	}
	
	public void onListsEnabledToggle()
	 {
		sendNotification(Notifications.PREFERENCES_CHANGE_LISTS_ENABLED, isListsEnabled());
	 }
	
	
	// UTILITY
	public String getLangCode() {
		if (!langName) return getLanguage();
		else return MinorityTranslateModel.wikis().getLangCode(getLanguage());
	}

}
