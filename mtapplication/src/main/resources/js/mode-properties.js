define("ace/mode/properties_highlight_rules",["require","exports","module","ace/lib/oop","ace/mode/text_highlight_rules"], function(require, exports, module) {
"use strict";

var oop = require("../lib/oop");
var TextHighlightRules = require("./text_highlight_rules").TextHighlightRules;

var PropertiesHighlightRules = function() {

    this.$rules = {
        "start" : [
            {
                token : "markup.heading.1",
                regex : /^=[^=](?:.*?)[^=]=$/
            }, {
                token : "markup.heading.2",
                regex : /^==[^=](?:.*?)[^=]==$/
            }, {
                token : "markup.heading.3",
                regex : /^===[^=](?:.*?)[^=]===$/
            }, {
                token : "markup.heading.4",
                regex : /^====[^=](?:.*?)[^=]====$/
            }, {
                token : "markup.heading.5",
                regex : /^=====[^=](?:.*?)[^=]=====$/
            }, {
                token : "markup.heading.6",
                regex : /^======[^=](?:.*?)[^=]======$/
            }, {
                token : "bold",
                regex : /'''(?:.*?)'''/
            }, {
                token : "italic",
                regex : /''(?:.*?)''/
            }, {
                token : "comment",
                regex : /<!--(?:.*?)-->/
            }, {
                token : "function",
                regex : /\{\{/,
                next  : "template"
            }, {
                token : "function",
                regex : /\[\[/,
                next  : "link.internal"
            }, {
                token : "text",
                regex : /<\//
            }, {
                token : "function",
                regex : /</,
                next  : "tag.internal"
            }, {
                defaultToken: "text"
            }, {
                token : "constant.character",
                regex : /\&(?:.*?);/
            }
        ],
        "template" : [
            {
                regex : /\}\}/,
                token : "function",
                next : "start"
            }, {
                regex : /[ ]*\|[ ]*/,
                token : "function",
                next : "template.parameters"
            }, {
                defaultToken: "keyword"
            }
        ],
       "template.parameters" : [
            {
                regex : /\}\}/,
                token : "support.function",
                next : "start"
            }, {
                regex : /\{\{/,
                token : "function",
                next : "template.parameters.subtemplate"
            }, {
                regex : /[A-Za-z0-9_ ]+=/,
                token : "variable",
                next : "template.parameter.value"
            }, {
                regex : /[ ]*\|[ ]*/,
                token : "support.function"
            }, {
                defaultToken: "string"
            }
        ],
       "template.parameter.value" : [
            {
                regex : /\}\}/,
                token : "support.function",
                next : "start"
            }, {
                regex : /\{\{/,
                token : "function",
                next : "template.parameters.subtemplate"
            }, {
                regex : /[ ]*\|[ ]*/,
                token : "support.function",
                next : "template.parameters"
            }, {
                defaultToken: "string"
            }
        ],
        "template.parameters.subtemplate" : [
            {
                regex : /\}\}/,
                token : "function22",
                next : "template.parameters"
            }, {
                defaultToken: "sub"
            }
        ],
        "link.internal" : [
            {
                regex : /\]\]/,
                token : "function",
                next : "start"
            }, {
                regex : /(Main|User|Wikipedia|File|Mediawiki|Template|Help|Category|Portal|Book|Draft|Education Program|TimedText|Module|Topic):/,
                token : "keyword"
            }, {
                regex : /\|/,
                token : "function",
                next  : "link.internal.parameters"
            }, {
                defaultToken: "string"
            }
        ],
        "link.internal.parameters" : [
            {
                regex : /\]\]/,
                token : "function",
                next : "start"
            }, {
                regex : /\|/,
                token : "function"
            }, {
                defaultToken: "string"
            }
        ],
        "tag.internal" : [
            {
                regex : / \/\>/,
                token : "function",
                next : "start"
            }, {
                regex : / /,
                token : "function",
                next : "tag.internal.attributes"
            }, {
                regex : /\>/,
                token : "function",
                next : "tag.content"
            }, {
                defaultToken: "keyword"
            }
        ],
        "tag.internal.attributes" : [
            {
                regex : / *\/\>/,
                token : "function",
                next : "start"
            }, {
                regex : /\>/,
                token : "function",
                next : "tag.content"
            }, {
                regex : / /,
                token : "function"
            }, {
                regex : /[A-Za-z0-9_]+=/,
                token : "variable",
                next : "tag.internal.attribute.value"
            }, {
                regex : /"(?:.*?)"/,
                token : "string"
            }, {
                defaultToken: "invalid"
            }
        ],
        "tag.internal.attribute.value" : [
            {
                regex : / *\/\>/,
                token : "function",
                next : "start"
            }, {
                regex : /\>/,
                token : "function",
                next : "tag.content"
            }, {
                regex : /"/,
                token : "string",
                next : "tag.internal.attribute.value.string"
            }, {
                regex : / /,
                token : "function",
                next : "tag.internal.attributes"
            }, {
                defaultToken: "string"
            }
        ],"tag.internal.attribute.value.string" : [
            {
                regex : / *\/\>/,
                token : "invalid",
                next : "start"
            }, {
                regex : /\>/,
                token : "invalid",
                next : "tag.content"
            }, {
                regex : /"/,
                token : "string",
                next : "tag.internal.attribute.value"
            }, {
                defaultToken: "string"
            }
        ],
        "tag.content" : [
            {
                regex : / \/\>/,
                token : "function",
                next : "start"
            }, {
                regex : /</,
                token : "function",
                next : "tag.closing"
            }, {
                defaultToken: "string.other"
            }
        ], "tag.closing" : [
            {
                regex : />/,
                token : "function",
                next : "start"
            }, {
                defaultToken: "keyword"
            }
        ],
    };

};

oop.inherits(PropertiesHighlightRules, TextHighlightRules);

exports.PropertiesHighlightRules = PropertiesHighlightRules;
});

define("ace/mode/properties",["require","exports","module","ace/lib/oop","ace/mode/text","ace/mode/properties_highlight_rules"], function(require, exports, module) {
"use strict";

var oop = require("../lib/oop");
var TextMode = require("./text").Mode;
var PropertiesHighlightRules = require("./properties_highlight_rules").PropertiesHighlightRules;

var Mode = function() {
    this.HighlightRules = PropertiesHighlightRules;
};
oop.inherits(Mode, TextMode);

(function() {
    this.$id = "ace/mode/properties";
}).call(Mode.prototype);

exports.Mode = Mode;
});
